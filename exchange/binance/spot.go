package binance

import (
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/gorilla/websocket"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/comp_acc"
	"gitlab.com/msex/comp_acc/exchange/binance/define/spot"
	"gitlab.com/msex/define"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"google.golang.org/protobuf/proto"
	"sync"
	"sync/atomic"
	"time"
)

func init() {
	account.Reg(&Spot{
		symbolIdx: make(map[string]*m_symbol.Symbol),
	})
}

type Spot struct {
	symbolIdx map[string]*m_symbol.Symbol
	counter   int64
	chClose   sync.Map
}

func (p *Spot) Ty() m_common.Exchange {
	return m_common.Exchange_EX_Binance
}

func (p *Spot) ExTy() m_common.ExchangeType {
	return m_common.ExchangeType_ET_Spot
}

func (p *Spot) Start() error {
	rsp, err := resty.New().R().Get("https://api.binance.com/api/v3/exchangeInfo")
	if err != nil {
		log.Error("获取交易规则失败: ", err)
		return err
	}

	// 统计所有Symbol
	model := new(spot.InstrumentFromApi)
	err = util.JSONUnmarshal(rsp.Body(), model)
	if err != nil {
		log.Error("解析交易规则失败: ", err)
		return err
	}

	p.symbolIdx = model.ToSymbolIdx()
	return nil
}

func (p *Spot) Sub(apiKey, secret, _ string) (*account.Subscribed, error) {
	sub := &account.Subscribed{
		Id:       atomic.AddInt64(&p.counter, 1),
		Position: make(chan []*m_position.Position, 1024),
		Balance:  make(chan []*m_balance.Balance, 1024),
		Order:    make(chan *m_order.Order, 1024),
		Leverage: make(chan []*m_setting.Leverage, 1024),
		Error:    make(chan error, 1),
	}

	chClose := make(chan bool, 1)
	p.chClose.Store(sub.Id, chClose)

	go p.sub(apiKey, secret, sub, chClose)
	return sub, nil
}

func (p *Spot) UnSub(subId int64) {
	ch, ok := p.chClose.LoadAndDelete(subId)
	if !ok {
		return
	}

	ch.(chan bool) <- true
}

func (p *Spot) ChangeSetting(string, string, string, []*m_setting.Leverage) error {
	return define.ErrNotSupport
}

func (p *Spot) sub(apiKey, secret string, sub *account.Subscribed, close chan bool) {
	// 自动重连
	for {
		// 获取key
		cli := resty.New()
		rsp, err := cli.R().
			SetHeader("X-MBX-APIKEY", apiKey).
			Post("https://api.binance.com/api/v3/userDataStream")
		if err != nil {
			log.Error("获取listenKey失败:", err)
			<-time.After(time.Second * 5)
			continue
		}

		m := struct {
			Key string `json:"listenKey"`
		}{}

		err = util.JSONUnmarshal(rsp.Body(), &m)
		if err != nil {
			log.Error("获取listenKey失败:", err)
			<-time.After(time.Second * 5)
			continue
		}

		if len(m.Key) == 0 {
			log.Error("获取listenKey失败.")
			<-time.After(time.Second * 5)
			continue
		}

		// 根据key创建WS流
		c, _, err := websocket.DefaultDialer.Dial(
			fmt.Sprintf("wss://stream.binance.com:9443/ws/%s", m.Key),
			nil)
		if err != nil {
			log.Warn("连接ws流失败:", err)
			<-time.After(time.Second * 5)
			continue
		}

		// 人工关闭标记
		var manualClose int32
		// 定期更新ws流
		timer := time.NewTicker(time.Second * 60 * 19)
		go func() {
			for {
				select {
				case <-close:
					atomic.StoreInt32(&manualClose, 1)
					_ = c.Close()
					timer.Stop()
					return
				case <-timer.C:
					rsp, err = cli.R().
						SetHeader("X-MBX-APIKEY", apiKey).
						Put("https://api.binance.com/api/v3/userDataStream")
					if err != nil {
						log.Warn("renew listenKey失败:", err)
					} else {
						log.Info("renew listenKey")
					}
				}
			}
		}()

		// 全同步一次
		bal, err := spot.RestfulBal(apiKey, secret)
		if err != nil {
			sub.Error <- err
			return
		}

		sub.Balance <- bal

		// 维护全量数据
		fulBal := make(map[string]*m_balance.Balance)
		for _, balance := range bal {
			fulBal[balance.Symbol.String()] = balance
		}

		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Warn("读取消息失败:", err)
				break
			}

			ev := make(map[string]interface{})

			// 先分析事件类型
			err = util.JSONUnmarshal(message, &ev)
			if err != nil {
				log.Warn("解析消息失败:", err)
				continue
			}

			switch util.ToString(ev["e"]) {
			case "outboundAccountPosition":
				p.onUpdateAcc(message, sub, fulBal)
			case "executionReport":
				p.onUpdateOrder(message, sub)
			}
		}

		if atomic.LoadInt32(&manualClose) != 0 {
			return
		}
	}
}

func (p *Spot) onUpdateOrder(message []byte, sub *account.Subscribed) {
	o, err := spot.StreamOrder(message, p.symbolIdx)
	if err != nil {
		log.Warn("解析消息失败:", err)
		return
	}

	// 可能不是适配的数据，忽略为nil
	if o == nil {
		return
	}

	sub.Order <- o
}

func (p *Spot) onUpdateAcc(message []byte, sub *account.Subscribed, fulBal map[string]*m_balance.Balance) {
	bal, err := spot.StreamAcc(message)
	if err != nil {
		sub.Error <- err
		return
	}

	if bal != nil {
		// 合并余额
		for _, balance := range bal {
			fulBal[balance.Symbol.String()] = balance
		}

		// 全copy
		var b []*m_balance.Balance
		for ky, balance := range fulBal {
			if balance.Balance == 0 {
				delete(fulBal, ky)
				continue
			}

			b = append(b, proto.Clone(balance).(*m_balance.Balance))
		}

		sub.Balance <- b
	}
}
