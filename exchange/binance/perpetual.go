package binance

import (
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/gorilla/websocket"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/comp_acc"
	"gitlab.com/msex/comp_acc/exchange/binance/define/perpetual"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"google.golang.org/protobuf/proto"
	"sync"
	"sync/atomic"
	"time"
)

func init() {
	account.Reg(&Perpetual{
		symbolIdx: make(map[string]*m_symbol.Symbol),
	})
}

type Perpetual struct {
	symbolIdx map[string]*m_symbol.Symbol
	counter   int64
	chClose   sync.Map
}

func (p *Perpetual) Ty() m_common.Exchange {
	return m_common.Exchange_EX_Binance
}

func (p *Perpetual) ExTy() m_common.ExchangeType {
	return m_common.ExchangeType_ET_Perpetual
}

func (p *Perpetual) Start() error {
	rsp, err := resty.New().R().Get("https://fapi.binance.com/fapi/v1/exchangeInfo")
	if err != nil {
		log.Error("获取交易规则失败: ", err)
		return err
	}

	// 统计所有Symbol
	model := new(perpetual.InstrumentFromApi)
	err = util.JSONUnmarshal(rsp.Body(), model)
	if err != nil {
		log.Error("解析交易规则失败: ", err)
		return err
	}

	p.symbolIdx = model.ToSymbolIdx()
	return nil
}

func (p *Perpetual) Sub(apiKey, secret, _ string) (*account.Subscribed, error) {
	sub := &account.Subscribed{
		Id:       atomic.AddInt64(&p.counter, 1),
		Position: make(chan []*m_position.Position, 1024),
		Balance:  make(chan []*m_balance.Balance, 1024),
		Order:    make(chan *m_order.Order, 1024),
		Leverage: make(chan []*m_setting.Leverage, 1024),
		Error:    make(chan error, 1),
	}

	chClose := make(chan bool, 1)
	p.chClose.Store(sub.Id, chClose)

	go p.sub(apiKey, secret, sub, chClose)
	return sub, nil
}

func (p *Perpetual) UnSub(subId int64) {
	ch, ok := p.chClose.LoadAndDelete(subId)
	if !ok {
		return
	}

	ch.(chan bool) <- true
}

func (p *Perpetual) ChangeSetting(apiKey, secret, _ string, setting []*m_setting.Leverage) error {
	for _, leverage := range setting {
		err := perpetual.RestfulChangeLeverage(apiKey, secret, leverage.Symbol, int(leverage.Lv))
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *Perpetual) sub(apiKey, secret string, sub *account.Subscribed, close chan bool) {
	// 自动重连
	for {
		// 获取key
		cli := resty.New()
		rsp, err := cli.R().
			SetHeader("X-MBX-APIKEY", apiKey).
			Post("https://fapi.binance.com/fapi/v1/listenKey")
		if err != nil {
			log.Error("获取listenKey失败:", err)
			<-time.After(time.Second * 5)
			continue
		}

		m := struct {
			Key string `json:"listenKey"`
		}{}

		err = util.JSONUnmarshal(rsp.Body(), &m)
		if err != nil {
			log.Error("获取listenKey失败:", err)
			<-time.After(time.Second * 5)
			continue
		}

		if len(m.Key) == 0 {
			log.Error("获取listenKey失败.")
			<-time.After(time.Second * 5)
			continue
		}

		// 根据key创建WS流
		c, _, err := websocket.DefaultDialer.Dial(
			fmt.Sprintf("wss://fstream.binance.com/ws/%s", m.Key),
			nil)
		if err != nil {
			log.Warn("连接ws流失败:", err)
			<-time.After(time.Second * 5)
			continue
		}

		// 人工关闭标记
		var manualClose int32
		// 定期更新ws流
		timer := time.NewTicker(time.Second * 60 * 19)
		go func() {
			for {
				select {
				case <-close:
					atomic.StoreInt32(&manualClose, 1)
					_ = c.Close()
					timer.Stop()
					return
				case <-timer.C:
					rsp, err = cli.R().
						SetHeader("X-MBX-APIKEY", apiKey).
						Put("https://fapi.binance.com/fapi/v1/listenKey")
					if err != nil {
						log.Warn("renew listenKey失败:", err)
					} else {
						log.Info("renew listenKey")
					}
				}
			}
		}()

		// 全同步一次
		bal, pos, err := perpetual.RestfulPosAndBal(apiKey, secret, p.symbolIdx)
		if err != nil {
			sub.Error <- err
			return
		}

		lv, err := perpetual.RestfulLeverage(apiKey, secret, p.symbolIdx)
		if err != nil {
			sub.Error <- err
			return
		}

		sub.Position <- pos
		sub.Balance <- bal
		sub.Leverage <- lv

		// 维护全量数据
		fulBal := make(map[string]*m_balance.Balance)
		for _, balance := range bal {
			fulBal[balance.Symbol.String()] = balance
		}

		fulPos := make(map[string]*m_position.Position)
		for _, po := range pos {
			fulPos[fmt.Sprintf("%s-%d", po.Symbol.String(), po.Side)] = po
		}

		fulLv := make(map[string]*m_setting.Leverage)
		for _, leverage := range lv {
			fulLv[leverage.String()] = leverage
		}

		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Warn("读取消息失败:", err)
				break
			}

			ev := make(map[string]interface{})

			// 先分析事件类型
			err = util.JSONUnmarshal(message, &ev)
			if err != nil {
				log.Warn("解析消息失败:", err)
				continue
			}

			switch util.ToString(ev["e"]) {
			case "ACCOUNT_UPDATE":
				p.onUpdateAcc(message, sub, fulBal, fulPos)
			case "ORDER_TRADE_UPDATE":
				p.onUpdateOrder(message, sub)
			case "ACCOUNT_CONFIG_UPDATE":
				p.onUpdateConfig(message, fulLv, sub)
			}
		}

		if atomic.LoadInt32(&manualClose) != 0 {
			return
		}
	}
}

func (p *Perpetual) onUpdateConfig(message []byte, fulLv map[string]*m_setting.Leverage, sub *account.Subscribed) {
	l, err := perpetual.StreamConfig(message, p.symbolIdx)
	if err != nil {
		log.Warn("解析消息失败:", err)
		return
	}

	// 可能不是适配的数据，忽略为nil
	if l == nil {
		return
	}

	fulLv[l.Symbol.String()] = l
	// 全copy
	s := make([]*m_setting.Leverage, len(fulLv))
	idx := 0
	for _, leverage := range fulLv {
		s[idx] = &m_setting.Leverage{
			Symbol: leverage.Symbol,
			Lv:     leverage.Lv}
		idx++
	}

	// 全量更新
	sub.Leverage <- s
}

func (p *Perpetual) onUpdateOrder(message []byte, sub *account.Subscribed) {
	o, err := perpetual.StreamOrder(message, p.symbolIdx)
	if err != nil {
		log.Warn("解析消息失败:", err)
		return
	}

	// 可能不是适配的数据，忽略为nil
	if o == nil {
		return
	}

	sub.Order <- o
}

func (p *Perpetual) onUpdateAcc(message []byte, sub *account.Subscribed, fulBal map[string]*m_balance.Balance, fulPos map[string]*m_position.Position) {
	bal, pos, err := perpetual.StreamPosAndBal(message, p.symbolIdx)
	if err != nil {
		sub.Error <- err
		return
	}

	if bal != nil {
		// 合并余额
		for _, balance := range bal {
			fulBal[balance.Symbol.String()] = balance
		}

		// 全copy
		var b []*m_balance.Balance
		for ky, balance := range fulBal {
			if balance.Balance == 0 {
				delete(fulBal, ky)
				continue
			}

			b = append(b, proto.Clone(balance).(*m_balance.Balance))
		}

		sub.Balance <- b
	}

	if pos != nil {
		// 合并仓位
		for _, po := range pos {
			fulPos[fmt.Sprintf("%s-%d", po.Symbol.String(), po.Side)] = po
		}

		// 全copy
		var p []*m_position.Position
		for ky, po := range fulPos {
			if po.Quantity == 0 {
				delete(fulPos, ky)
				continue
			}

			p = append(p, proto.Clone(po).(*m_position.Position))
		}

		sub.Position <- p
	}
}
