package spot

import (
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"math"
	"strings"
)

type InstrumentSymbol struct {
	Symbol     string                   `json:"symbol"`
	BaseAsset  string                   `json:"baseAsset"`
	QuoteAsset string                   `json:"quoteAsset"`
	Filters    []map[string]interface{} `json:"filters"` // 需要 filterType = PRICE_FILTER LOT_SIZE
}

type InstrumentFromApi struct {
	Symbols []*InstrumentSymbol `json:"symbols"`
}

func (i *InstrumentFromApi) ToMapInstrument() map[string]*Instrument {
	m := make(map[string]*Instrument)
	for _, sym := range i.Symbols {
		var (
			pp float64
			qp float64
		)

		for _, f := range sym.Filters {
			switch util.ToString(f["filterType"]) {
			case "PRICE_FILTER":
				pp = util.ToFloat64(f["tickSize"])
			case "LOT_SIZE":
				qp = util.ToFloat64(f["stepSize"])
			}
		}

		m[sym.Symbol] = &Instrument{
			PricePrecision:    pp,
			QuantityPrecision: qp,
		}
	}

	return m
}

func (i *InstrumentFromApi) ToSymbolIdx() map[string]*m_symbol.Symbol {
	m := make(map[string]*m_symbol.Symbol)
	for _, symbol := range i.Symbols {
		m[strings.ToLower(symbol.Symbol)] = &m_symbol.Symbol{
			Base:  strings.ToLower(symbol.BaseAsset),
			Quote: strings.ToLower(symbol.QuoteAsset),
		}
	}

	return m
}

type Instrument struct {
	PricePrecision    float64 // 价格精度
	QuantityPrecision float64 // 数量精度
}

func (i *Instrument) Price(p float64) float64 {
	if i.PricePrecision >= 1 {
		return float64(int64(p) / int64(i.PricePrecision) * int64(i.PricePrecision))
	}

	mul := math.Pow10(int(math.Log10(1.0 / i.PricePrecision)))
	return math.Round(p*mul) / mul
}

func (i *Instrument) Quantity(q float64) float64 {
	if i.QuantityPrecision >= 1 {
		return float64(int64(q) / int64(i.QuantityPrecision) * int64(i.QuantityPrecision))
	}

	mul := math.Pow10(int(math.Log10(1.0 / i.QuantityPrecision)))
	return math.Round(q*mul) / mul
}
