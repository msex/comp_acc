package spot

import (
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"strings"
)

type streamAccount struct {
	EvTy    string `json:"e"`
	EvTime  int64  `json:"E"`
	Balance []*struct {
		Symbol string  `json:"a"`
		Amount float64 `json:"f,string"`
	} `json:"B"`
}

func (a *streamAccount) toPB() ([]*m_balance.Balance, error) {
	var (
		b = make([]*m_balance.Balance, len(a.Balance))
	)

	for i, balance := range a.Balance {
		b[i] = &m_balance.Balance{
			Symbol: &m_symbol.Symbol{
				Base: strings.ToLower(balance.Symbol),
			},
			Balance: balance.Amount,
		}
	}

	return b, nil
}

func StreamAcc(data []byte) ([]*m_balance.Balance, error) {
	a := new(streamAccount)
	err := util.JSONUnmarshal(data, a)
	if err != nil {
		return nil, err
	}

	return a.toPB()
}

type streamBalance struct {
	Symbol string  `json:"a"`
	Amount float64 `json:"d,string"`
}

func (a *streamBalance) toPB() ([]*m_balance.Balance, error) {
	b := []*m_balance.Balance{
		{
			Symbol: &m_symbol.Symbol{
				Base: strings.ToLower(a.Symbol),
			},
			Balance: a.Amount,
		},
	}

	return b, nil
}

func StreamBal(data []byte) ([]*m_balance.Balance, error) {
	a := new(streamBalance)
	err := util.JSONUnmarshal(data, a)
	if err != nil {
		return nil, err
	}

	return a.toPB()
}

type streamOrder struct {
	Id             string  `json:"c"`
	OTy            string  `json:"o"`
	Symbol         string  `json:"s"`
	OrderSide      string  `json:"S"`
	Quantity       float64 `json:"q,string"`
	LatestQuantity float64 `json:"l,string"`
	TotalQuantity  float64 `json:"z,string"`
	Price          float64 `json:"p,string"`
	LatestPrice    float64 `json:"L,string"`
	AvgPrice       float64 `json:"-"` // 现货订单没有成交均价
	TransPrice     float64 `json:"Q,string"`
	TransExecPrice float64 `json:"Z,string"`
	FeeSymbol      string  `json:"N"`
	Fee            float64 `json:"n,string"`
	Status         string  `json:"X"`
	Ts             int64   `json:"T"`
}

func (s *streamOrder) toPB(symIdx map[string]*m_symbol.Symbol) *m_order.Order {
	var (
		eTy          = m_common.ExchangeType_ET_Spot
		oTy          m_order.OrderType
		symbol       *m_symbol.Symbol
		positionSide = m_position.PositionSide_PS_Long
		orderSide    m_order.OrderSide
	)

	switch s.OTy {
	case "MARKET":
		oTy = m_order.OrderType_OT_Market
	case "LIMIT":
		oTy = m_order.OrderType_OT_Limit
	case "STOP":
		oTy = m_order.OrderType_OT_Stop
	case "TAKE_PROFIT":
		oTy = m_order.OrderType_OT_TakeProfit
	default:
		// 不支持的订单类型
		return nil
	}

	var ok bool
	symbol, ok = symIdx[strings.ToLower(s.Symbol)]
	if !ok {
		// 未知symbol
		return nil
	}

	switch s.OrderSide {
	case "BUY":
		orderSide = m_order.OrderSide_OS_Buy
	case "SELL":
		orderSide = m_order.OrderSide_OS_Sell
	default:
		// 不支持的买卖类型
		return nil
	}

	return &m_order.Order{
		Id:             s.Id,
		Ex:             m_common.Exchange_EX_Binance,
		ETy:            eTy,
		OTy:            oTy,
		Symbol:         symbol,
		PositionSide:   positionSide,
		OrderSide:      orderSide,
		Quantity:       s.Quantity,
		LatestQuantity: s.LatestQuantity,
		TotalQuantity:  s.TotalQuantity,
		Price:          s.Price,
		LatestPrice:    s.LatestPrice,
		AvgPrice:       s.AvgPrice,
		TransPrice:     s.TransPrice,
		TransExecPrice: s.TransExecPrice,
		FeeSymbol:      strings.ToLower(s.FeeSymbol),
		Fee:            s.Fee,
		Finished:       s.Status == "FILLED",
		Ts:             s.Ts,
	}
}

func StreamOrder(data []byte, symIdx map[string]*m_symbol.Symbol) (*m_order.Order, error) {
	// TODO: 区分新单，成交单，撤销单
	o := new(streamOrder)
	err := util.JSONUnmarshal(data, o)
	if err != nil {
		return nil, err
	}

	return o.toPB(symIdx), nil
}
