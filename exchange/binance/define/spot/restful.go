package spot

import (
	"errors"
	"fmt"
	"github.com/go-resty/resty/v2"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/define"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"strings"
	"time"
)

type restfulAsset struct {
	Symbol string  `json:"asset"`
	Amount float64 `json:"free,string"`
}

type restfulAccount struct {
	Assets []*restfulAsset `json:"balances"`
}

func (r *restfulAccount) toPB() ([]*m_balance.Balance, error) {
	var (
		b []*m_balance.Balance
	)

	for _, asset := range r.Assets {
		if asset.Amount == 0 {
			continue
		}

		b = append(b, &m_balance.Balance{
			Symbol: &m_symbol.Symbol{
				Base: strings.ToLower(asset.Symbol),
			},
			Balance: asset.Amount,
		})
	}

	return b, nil
}

type restfulPositionRisk struct {
	Symbol   string `json:"symbol"`
	Leverage int    `json:"leverage,string"`
	Update   int64  `json:"updateTime"`
}

func RestfulBal(apiKey, secret string) ([]*m_balance.Balance, error) {
	// 通过restful拿持仓和余额全数据
	now := time.Now()
	sign := util.HmacSha256Hex(fmt.Sprintf("recvWindow=5000&timestamp=%d", now.UnixMilli()), secret)

	cli := resty.New()
	rsp, err := cli.R().
		SetHeader("X-MBX-APIKEY", apiKey).
		SetQueryParam("recvWindow", "5000").
		SetQueryParam("timestamp", util.ToString(now.UnixMilli())).
		SetQueryParam("signature", sign).
		Get("https://api.binance.com/api/v3/account")
	if err != nil {
		return nil, err
	}

	ra := new(restfulAccount)
	err = util.JSONUnmarshal(rsp.Body(), ra)
	if err != nil {
		return nil, err
	}

	return ra.toPB()
}

func RestfulLeverage(apiKey, secret string, symbolIdx map[string]*m_symbol.Symbol) ([]*m_setting.Leverage, error) {
	// 通过restful拿杠杆全数据
	now := time.Now()
	sign := util.HmacSha256Hex(fmt.Sprintf("recvWindow=5000&timestamp=%d", now.UnixMilli()), secret)

	cli := resty.New()
	rsp, err := cli.R().
		SetHeader("X-MBX-APIKEY", apiKey).
		SetQueryParam("recvWindow", "5000").
		SetQueryParam("timestamp", util.ToString(now.UnixMilli())).
		SetQueryParam("signature", sign).
		Get("https://fapi.binance.com/fapi/v2/positionRisk")
	if err != nil {
		return nil, err
	}

	var (
		arr []*restfulPositionRisk
		lv  []*m_setting.Leverage
	)

	err = util.JSONUnmarshal(rsp.Body(), &arr)
	if err != nil {
		return nil, err
	}

	tm := make(map[string]int64)
	for _, risk := range arr {
		symbol, ok := symbolIdx[strings.ToLower(risk.Symbol)]
		if !ok {
			return nil, define.ErrUnknownSymbol
		}

		t := tm[risk.Symbol]
		if risk.Update <= t {
			// 时间老的杠杆记录不覆盖
			continue
		}

		tm[risk.Symbol] = risk.Update
		lv = append(lv, &m_setting.Leverage{
			Symbol: symbol,
			Lv:     int32(risk.Leverage),
		})
	}

	return lv, nil
}

func RestfulChangeLeverage(apiKey, secret string, symbol *m_symbol.Symbol, val int) error {
	sym := strings.ToUpper(fmt.Sprintf("%s%s", symbol.Base, symbol.Quote))

	// 通过restful拿杠杆全数据
	now := time.Now()
	sign := util.HmacSha256Hex(fmt.Sprintf("symbol=%s&leverage=%drecvWindow=5000&timestamp=%d", sym, val, now.UnixMilli()), secret)

	cli := resty.New()
	rsp, err := cli.R().
		SetHeader("X-MBX-APIKEY", apiKey).
		SetQueryParam("symbol", sym).
		SetQueryParam("leverage", util.ToString(val)).
		SetQueryParam("recvWindow", "5000").
		SetQueryParam("timestamp", util.ToString(now.UnixMilli())).
		SetQueryParam("signature", sign).
		Post("https://fapi.binance.com/fapi/v1/leverage")
	if err != nil {
		return err
	}

	m := struct {
		Code int    `json:"code"`
		Msg  string `json:"msg"`
	}{}

	err = util.JSONUnmarshal(rsp.Body(), &m)
	if err != nil {
		return err
	}

	if m.Code != 200 {
		return errors.New(m.Msg)
	}

	return nil
}
