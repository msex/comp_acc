package perpetual

import (
	"errors"
	"fmt"
	"github.com/go-resty/resty/v2"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/define"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"strings"
	"time"
)

type restfulAsset struct {
	Symbol string  `json:"asset"`
	Amount float64 `json:"walletBalance,string"`
}

type restfulPosition struct {
	Symbol       string  `json:"symbol"`
	Amount       float64 `json:"positionAmt,string"`
	Price        float64 `json:"entryPrice,string"`
	PositionSide string  `json:"positionSide"`
	Lv           int     `json:"leverage,string"`
	Isolated     bool    `json:"isolated"`
}

type restfulAccount struct {
	Assets    []*restfulAsset    `json:"assets"`
	Positions []*restfulPosition `json:"positions"`
}

func (r *restfulAccount) toPB(symbolIdx map[string]*m_symbol.Symbol) ([]*m_balance.Balance, []*m_position.Position, error) {
	var (
		b []*m_balance.Balance
		p []*m_position.Position
	)

	for _, asset := range r.Assets {
		if asset.Amount == 0 {
			continue
		}

		b = append(b, &m_balance.Balance{
			Symbol: &m_symbol.Symbol{
				Base: strings.ToLower(asset.Symbol),
			},
			Balance: asset.Amount,
		})
	}

	for _, pos := range r.Positions {
		if pos.Amount == 0 {
			continue
		}

		// TODO: 支持逐仓
		if pos.Isolated == true {
			continue
		}

		symbol, ok := symbolIdx[strings.ToLower(pos.Symbol)]
		if !ok {
			return nil, nil, define.ErrUnknownSymbol
		}

		var ps m_position.PositionSide
		switch pos.PositionSide {
		case "BOTH", "LONG":
			ps = m_position.PositionSide_PS_Long
		case "SHORT":
			ps = m_position.PositionSide_PS_Short
		}

		var mt m_setting.MarginType
		if pos.Isolated {
			mt = m_setting.MarginType_MT_ISOLATED
		} else {
			mt = m_setting.MarginType_MT_CROSSED
		}

		p = append(p, &m_position.Position{
			Symbol:   symbol,
			Side:     ps,
			Quantity: pos.Amount,
			Price:    pos.Price,
			Mt:       mt,
		})
	}

	return b, p, nil
}

type restfulPositionRisk struct {
	Symbol   string `json:"symbol"`
	Leverage int    `json:"leverage,string"`
	Update   int64  `json:"updateTime"`
}

func RestfulPosAndBal(apiKey, secret string, symbolIdx map[string]*m_symbol.Symbol) ([]*m_balance.Balance, []*m_position.Position, error) {
	// 通过restful拿持仓和余额全数据
	now := time.Now()
	sign := util.HmacSha256Hex(fmt.Sprintf("recvWindow=5000&timestamp=%d", now.UnixMilli()), secret)

	cli := resty.New()
	rsp, err := cli.R().
		SetHeader("X-MBX-APIKEY", apiKey).
		SetQueryParam("recvWindow", "5000").
		SetQueryParam("timestamp", util.ToString(now.UnixMilli())).
		SetQueryParam("signature", sign).
		Get("https://fapi.binance.com/fapi/v2/account")
	if err != nil {
		return nil, nil, err
	}

	ra := new(restfulAccount)
	err = util.JSONUnmarshal(rsp.Body(), ra)
	if err != nil {
		return nil, nil, err
	}

	return ra.toPB(symbolIdx)
}

func RestfulLeverage(apiKey, secret string, symbolIdx map[string]*m_symbol.Symbol) ([]*m_setting.Leverage, error) {
	// 通过restful拿杠杆全数据
	now := time.Now()
	sign := util.HmacSha256Hex(fmt.Sprintf("recvWindow=5000&timestamp=%d", now.UnixMilli()), secret)

	cli := resty.New()
	rsp, err := cli.R().
		SetHeader("X-MBX-APIKEY", apiKey).
		SetQueryParam("recvWindow", "5000").
		SetQueryParam("timestamp", util.ToString(now.UnixMilli())).
		SetQueryParam("signature", sign).
		Get("https://fapi.binance.com/fapi/v2/positionRisk")
	if err != nil {
		return nil, err
	}

	var (
		arr []*restfulPositionRisk
		lv  []*m_setting.Leverage
	)

	err = util.JSONUnmarshal(rsp.Body(), &arr)
	if err != nil {
		return nil, err
	}

	tm := make(map[string]int64)
	for _, risk := range arr {
		symbol, ok := symbolIdx[strings.ToLower(risk.Symbol)]
		if !ok {
			return nil, define.ErrUnknownSymbol
		}

		t := tm[risk.Symbol]
		if risk.Update <= t {
			// 时间老的杠杆记录不覆盖
			continue
		}

		tm[risk.Symbol] = risk.Update
		lv = append(lv, &m_setting.Leverage{
			Symbol: symbol,
			Lv:     int32(risk.Leverage),
		})
	}

	return lv, nil
}

func RestfulChangeLeverage(apiKey, secret string, symbol *m_symbol.Symbol, val int) error {
	sym := strings.ToUpper(fmt.Sprintf("%s%s", symbol.Base, symbol.Quote))

	// 通过restful拿杠杆全数据
	now := time.Now()
	sign := util.HmacSha256Hex(fmt.Sprintf("symbol=%s&leverage=%drecvWindow=5000&timestamp=%d", sym, val, now.UnixMilli()), secret)

	cli := resty.New()
	rsp, err := cli.R().
		SetHeader("X-MBX-APIKEY", apiKey).
		SetQueryParam("symbol", sym).
		SetQueryParam("leverage", util.ToString(val)).
		SetQueryParam("recvWindow", "5000").
		SetQueryParam("timestamp", util.ToString(now.UnixMilli())).
		SetQueryParam("signature", sign).
		Post("https://fapi.binance.com/fapi/v1/leverage")
	if err != nil {
		return err
	}

	m := struct {
		Code int    `json:"code"`
		Msg  string `json:"msg"`
	}{}

	err = util.JSONUnmarshal(rsp.Body(), &m)
	if err != nil {
		return err
	}

	if m.Code != 200 {
		return errors.New(m.Msg)
	}

	return nil
}
