package perpetual

import (
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/define"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"strings"
)

type streamBalance struct {
	Symbol string  `json:"a"`
	Amount float64 `json:"wb,string"`
}

type streamPosition struct {
	Symbol       string  `json:"s"`
	Amount       float64 `json:"pa,string"`
	Price        float64 `json:"ep,string"`
	PositionSide string  `json:"ps"`
	MarginType   string  `json:"mt"`
}

type streamAccount struct {
	EvTy    string `json:"e"`
	EvTime  int64  `json:"E"`
	Account struct {
		M        string            `json:"m"`
		Balance  []*streamBalance  `json:"B"`
		Position []*streamPosition `json:"P"`
	} `json:"a"`
}

func (a *streamAccount) toPB(idx map[string]*m_symbol.Symbol) ([]*m_balance.Balance, []*m_position.Position, error) {
	if a.Account.M == "FUNDING FEE" {
		// TODO: 想办法维护非全量数据
		return nil, nil, nil
	}

	var (
		b = make([]*m_balance.Balance, len(a.Account.Balance))
		p = make([]*m_position.Position, len(a.Account.Position))
	)

	for i, balance := range a.Account.Balance {
		b[i] = &m_balance.Balance{
			Symbol: &m_symbol.Symbol{
				Base: strings.ToLower(balance.Symbol),
			},
			Balance: balance.Amount,
		}
	}

	for i, position := range a.Account.Position {
		// TODO: 支持逐仓
		if position.MarginType == "isolated" {
			continue
		}

		sym, ok := idx[strings.ToLower(position.Symbol)]
		if !ok {
			log.Error(define.ErrUnknownSymbol)
			continue
		}

		var (
			side m_position.PositionSide
			mt   m_setting.MarginType
		)

		switch position.PositionSide {
		case "LONG", "BOTH":
			side = m_position.PositionSide_PS_Long
		case "SHORT":
			side = m_position.PositionSide_PS_Short
		default:
			continue
		}

		if position.MarginType == "isolated" {
			mt = m_setting.MarginType_MT_ISOLATED
		} else {
			mt = m_setting.MarginType_MT_CROSSED
		}

		p[i] = &m_position.Position{
			Symbol:   sym,
			Side:     side,
			Quantity: position.Amount,
			Price:    position.Price,
			Mt:       mt,
		}
	}

	return b, p, nil
}

func StreamPosAndBal(data []byte, symIdx map[string]*m_symbol.Symbol) ([]*m_balance.Balance, []*m_position.Position, error) {
	a := new(streamAccount)
	err := util.JSONUnmarshal(data, a)
	if err != nil {
		return nil, nil, err
	}

	return a.toPB(symIdx)
}

type streamOrder struct {
	Id             string  `json:"c"`
	OTy            string  `json:"o"`
	Symbol         string  `json:"s"`
	PositionSide   string  `json:"ps"`
	OrderSide      string  `json:"S"`
	Quantity       float64 `json:"q,string"`
	LatestQuantity float64 `json:"l,string"`
	TotalQuantity  float64 `json:"z,string"`
	Price          float64 `json:"p,string"`
	LatestPrice    float64 `json:"L,string"`
	AvgPrice       float64 `json:"ap,string"`
	FeeSymbol      string  `json:"N"`
	Fee            float64 `json:"n,string"`
	Status         string  `json:"X"`
}

type streamOrderWrap struct {
	Order *streamOrder `json:"o"`
	Ts    int64        `json:"T"`
}

func (s *streamOrderWrap) toPB(symIdx map[string]*m_symbol.Symbol) *m_order.Order {
	var (
		eTy          = m_common.ExchangeType_ET_Perpetual
		oTy          m_order.OrderType
		symbol       *m_symbol.Symbol
		positionSide m_position.PositionSide
		orderSide    m_order.OrderSide
	)

	switch s.Order.OTy {
	case "MARKET":
		oTy = m_order.OrderType_OT_Market
	case "LIMIT":
		oTy = m_order.OrderType_OT_Limit
	case "STOP":
		oTy = m_order.OrderType_OT_Stop
	case "TAKE_PROFIT":
		oTy = m_order.OrderType_OT_TakeProfit
	default:
		// 不支持的订单类型
		return nil
	}

	var ok bool
	symbol, ok = symIdx[strings.ToLower(s.Order.Symbol)]
	if !ok {
		// 未知symbol
		return nil
	}

	switch s.Order.PositionSide {
	case "LONG", "BOTH":
		positionSide = m_position.PositionSide_PS_Long
	case "SHORT":
		positionSide = m_position.PositionSide_PS_Short
	default:
		// 不支持的持仓类型
		return nil
	}

	switch s.Order.OrderSide {
	case "BUY":
		orderSide = m_order.OrderSide_OS_Buy
	case "SELL":
		orderSide = m_order.OrderSide_OS_Sell
	default:
		// 不支持的买卖类型
		return nil
	}

	return &m_order.Order{
		Id:             s.Order.Id,
		Ex:             m_common.Exchange_EX_Binance,
		ETy:            eTy,
		OTy:            oTy,
		Symbol:         symbol,
		PositionSide:   positionSide,
		OrderSide:      orderSide,
		Quantity:       s.Order.Quantity,
		LatestQuantity: s.Order.LatestQuantity,
		TotalQuantity:  s.Order.TotalQuantity,
		Price:          s.Order.Price,
		LatestPrice:    s.Order.LatestPrice,
		AvgPrice:       s.Order.AvgPrice,
		FeeSymbol:      strings.ToLower(s.Order.FeeSymbol),
		Fee:            s.Order.Fee,
		Finished:       s.Order.Status == "FILLED",
		Ts:             s.Ts,
	}
}

func StreamOrder(data []byte, symIdx map[string]*m_symbol.Symbol) (*m_order.Order, error) {
	// TODO: 区分新单，成交单，撤销单
	o := new(streamOrderWrap)
	err := util.JSONUnmarshal(data, o)
	if err != nil {
		return nil, err
	}

	return o.toPB(symIdx), nil
}

type streamLeverage struct {
	Symbol   string `json:"s"`
	Leverage int    `json:"l"`
}

type streamAccConfig struct {
	Ev string          `json:"e"`
	Lv *streamLeverage `json:"ac"`
}

func StreamConfig(data []byte, symbolIdx map[string]*m_symbol.Symbol) (*m_setting.Leverage, error) {
	ac := new(streamAccConfig)
	err := util.JSONUnmarshal(data, ac)
	if err != nil {
		return nil, err
	}

	if ac.Lv == nil {
		return nil, nil
	}

	sym, ok := symbolIdx[strings.ToLower(ac.Lv.Symbol)]
	if !ok {
		return nil, define.ErrUnknownSymbol
	}

	return &m_setting.Leverage{
		Symbol: sym,
		Lv:     int32(ac.Lv.Leverage),
	}, nil
}

//import (
//	define2 "gitlab.com/finance_comp/define"
//	"gitlab.com/flex_comp/log"
//	"gitlab.com/flex_comp/util"
//	"strings"
//	"time"
//)
//
//type OrderStream struct {
//	EvTy   string `json:"e"`
//	EvTime int64  `json:"E"`
//	Order  struct {
//		Id             string  `json:"c"`
//		Symbol         string  `json:"s"`
//		Side           string  `json:"S"`
//		Ty             string  `json:"o"`
//		PositionSide   string  `json:"ps"`
//		Quantity       float64 `json:"q,string"`
//		LatestQuantity float64 `json:"l,string"`
//		TotalQuantity  float64 `json:"z,string"`
//		Price          float64 `json:"p,string"`
//		LatestPrice    float64 `json:"L,string"`
//		AvgPrice       float64 `json:"ap,string"`
//	} `json:"o"`
//}
//
//func NewOrderFromStream(r []byte) (*define2.Order, error) {
//	o := new(OrderStream)
//	err := util.JSONUnmarshal(r, o)
//	if err != nil {
//		log.Warn("解析消息失败:", err)
//		return nil, err
//	}
//
//	return o.ToOrder(), nil
//}
//
//func (o *OrderStream) ToOrder() *define2.Order {
//	var (
//		ty           define.OrderType
//		side         define.Side
//		positionSide define.PositionSide
//	)
//
//	switch o.Order.Ty {
//	case "MARKET":
//		ty = define.Market
//	case "LIMIT":
//		ty = define.Limit
//	case "STOP":
//		ty = define.Stop
//	case "TAKE_PROFIT":
//		ty = define.TakeProfit
//	case "LIQUIDATION":
//		return nil
//	}
//
//	switch o.Order.Side {
//	case "BUY":
//		side = define.Buy
//	case "SELL":
//		side = define.Sell
//	}
//
//	switch o.Order.PositionSide {
//	case "BOTH", "LONG":
//		positionSide = define.Long
//	case "SHORT":
//		positionSide = define.Short
//	}
//
//	t := time.UnixMilli(o.EvTime)
//	return &define.Order{
//		Id:             o.Order.Id,
//		Symbol:         strings.ToLower(o.Order.Symbol),
//		Ty:             ty,
//		Side:           side,
//		PositionSide:   positionSide,
//		Quantity:       o.Order.Quantity,
//		LatestQuantity: o.Order.LatestQuantity,
//		TotalQuantity:  o.Order.TotalQuantity,
//		Price:          o.Order.Price,
//		LatestPrice:    o.Order.LatestPrice,
//		AvgPrice:       o.Order.AvgPrice,
//		Time:           &t,
//	}
//}
//
//type BalanceStream struct {
//	Symbol string  `json:"a"`
//	Amount float64 `json:"wb,string"`
//}
//
//type PositionStream struct {
//	Symbol       string  `json:"s"`
//	Amount       float64 `json:"pa,string"`
//	Price        float64 `json:"ep,string"`
//	PositionSide string  `json:"ps"`
//}
//
//type AccountStream struct {
//	EvTy    string `json:"e"`
//	EvTime  int64  `json:"E"`
//	Account struct {
//		Balance  []*BalanceStream  `json:"B"`
//		Position []*PositionStream `json:"P"`
//	} `json:"a"`
//}
//
//func (a *AccountStream) ToAccount() *define.Account {
//	m := define.NewAccount()
//	for _, b := range a.Account.Balance {
//		b.Symbol = strings.ToLower(b.Symbol)
//		m.Balance[b.Symbol] = &define.Balance{
//			Symbol: b.Symbol,
//			Amount: b.Amount,
//		}
//	}
//
//	for _, p := range a.Account.Position {
//		p.Symbol = strings.ToLower(p.Symbol)
//		mm, ok := m.Position[p.Symbol]
//		if !ok {
//			mm = make(map[define.PositionSide]*define.Position)
//			m.Position[p.Symbol] = mm
//		}
//
//		var positionSide define.PositionSide
//		switch p.PositionSide {
//		case "BOTH", "LONG":
//			positionSide = define.Long
//		case "SHORT":
//			positionSide = define.Short
//		}
//
//		mm[positionSide] = &define.Position{
//			Symbol:       p.Symbol,
//			Amount:       p.Amount,
//			Price:        p.Price,
//			PositionSide: positionSide,
//		}
//	}
//
//	return m
//}
//
//func parsePosAndBalFromStream(r []byte) ([]*define2.Balance, []*define2.Position, error) {
//	a := new(AccountStream)
//	err := util.JSONUnmarshal(r, a)
//	if err != nil {
//		log.Warn("解析消息失败:", err)
//		return nil, err
//	}
//
//	return a.ToAccount(), nil
//}
