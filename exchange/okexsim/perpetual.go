package okexsim

import (
	"errors"
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/gorilla/websocket"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/comp_acc"
	"gitlab.com/msex/comp_acc/exchange/okexsim/define"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"google.golang.org/protobuf/proto"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

func init() {
	account.Reg(&Perpetual{
		symbolIdx:  make(map[string]*m_symbol.Symbol),
		symbolInst: make(map[string]*define.SymbolItem),
	})
}

type Perpetual struct {
	symbolIdx  map[string]*m_symbol.Symbol
	symbolInst map[string]*define.SymbolItem
	counter    int64
	chClose    sync.Map
	mtxSend    sync.Mutex
}

func (p *Perpetual) Ty() m_common.Exchange {
	return m_common.Exchange_EX_OkExSim
}

func (p *Perpetual) ExTy() m_common.ExchangeType {
	return m_common.ExchangeType_ET_Perpetual
}

func (p *Perpetual) Start() error {
	cli := resty.New()
	rsp, err := cli.R().
		SetHeader("x-simulated-trading", "1").
		Get("https://www.okex.com/api/v5/public/instruments?instType=SWAP")
	if err != nil {
		log.Error("获取交易规则失败: ", err)
		return err
	}

	// 统计所有Symbol
	model := new(define.SymbolInstrument)
	err = util.JSONUnmarshal(rsp.Body(), model)
	if err != nil {
		log.Error("解析交易规则失败: ", err)
		return err
	}

	p.symbolIdx = model.ToSymbolIdx()
	p.symbolInst = model.ToMap()
	return nil
}

func (p *Perpetual) Sub(apiKey, secret, extended string) (*account.Subscribed, error) {
	sub := &account.Subscribed{
		Id:       atomic.AddInt64(&p.counter, 1),
		Position: make(chan []*m_position.Position, 1024),
		Balance:  make(chan []*m_balance.Balance, 1024),
		Order:    make(chan *m_order.Order, 1024),
		Leverage: make(chan []*m_setting.Leverage, 1024),
		Error:    make(chan error, 1),
	}

	chClose := make(chan bool, 1)
	p.chClose.Store(sub.Id, chClose)

	go p.sub(apiKey, secret, extended, sub, chClose)
	return sub, nil
}

func (p *Perpetual) UnSub(subId int64) {
	ch, ok := p.chClose.LoadAndDelete(subId)
	if !ok {
		return
	}

	ch.(chan bool) <- true
}

func (p *Perpetual) ChangeSetting(apiKey, secret, passphrase string, setting []*m_setting.Leverage) error {
	for _, leverage := range setting {
		err := define.RestfulChangeLeverage(apiKey, secret, passphrase, leverage.Symbol, int(leverage.Lv))
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *Perpetual) send(conn *websocket.Conn, data []byte) error {
	p.mtxSend.Lock()
	defer p.mtxSend.Unlock()

	return conn.WriteMessage(websocket.TextMessage, data)
}

func (p *Perpetual) sub(apiKey, secret, passphrase string, sub *account.Subscribed, close chan bool) {
	heartBeat := time.Second * 30
	pingTimer, pongTimer := time.NewTicker(time.Second*7), time.NewTicker(heartBeat)
	defer func() {
		pingTimer.Stop()
		pongTimer.Stop()
	}()

	baseUrl := "wss://wspap.okex.com:8443/ws/v5/private?brokerId=9999"

	// 自动重连
	for {
		c, _, err := websocket.DefaultDialer.Dial(baseUrl, nil)
		if err != nil {
			log.Warn("连接ws流失败:", err)
			<-time.After(time.Second * 5)
			continue
		}

		// 人工关闭标记
		var manualClose int32
		// 定期发PING, 关闭监听
		go p.healthCheck(c, pingTimer, pongTimer, &manualClose, close)

		// 登录
		go func() {
			err = p.login(apiKey, secret, passphrase, c)
			if err != nil {
				sub.Error <- err
			}
		}()

		fulBal := make(map[string]*m_balance.Balance)
		fulPos := make(map[string]*m_position.Position)

		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Warn("读取消息失败:", err)
				break
			}

			if string(message) == "pong" {
				pongTimer.Reset(heartBeat)
				continue
			}

			ev := make(map[string]interface{})

			// 先分析事件类型
			err = util.JSONUnmarshal(message, &ev)
			if err != nil {
				log.Warn("解析消息失败:", err)
				continue
			}

			// 事件
			switch util.ToString(ev["event"]) {
			case "login":
				err = p.loginAfter(c)
				if err != nil {
					sub.Error <- err
				}
			case "error":
				sub.Error <- errors.New(util.ToString(ev["msg"]))
			}

			if _, ok := ev["arg"]; !ok {
				continue
			}

			// 推送流
			channel := util.ToStringMap(ev["arg"])["channel"]
			switch util.ToString(channel) {
			case "balance_and_position":
				p.onUpdateBalAndPos(message, sub, fulBal, fulPos)
			case "orders":
				p.onUpdateOrder(message, sub)
			}
		}

		if atomic.LoadInt32(&manualClose) != 0 {
			return
		}
	}
}

func (p *Perpetual) onUpdateOrder(message []byte, sub *account.Subscribed) bool {
	o := define.StreamParseOrder(message, p.symbolIdx, p.symbolInst)
	if o == nil || o.ETy != m_common.ExchangeType_ET_Perpetual {
		return true
	}

	sub.Order <- o
	return false
}

func (p *Perpetual) onUpdateBalAndPos(message []byte, sub *account.Subscribed, fulBal map[string]*m_balance.Balance, fulPos map[string]*m_position.Position) {
	bal, pos := define.StreamParseBalAndPos(message, p.symbolIdx, p.symbolInst)
	if len(bal) > 0 {
		for _, ba := range bal {
			fulBal[strings.ToLower(ba.Symbol.String())] = ba
		}

		var dst []*m_balance.Balance
		for _, balance := range fulBal {
			dst = append(dst, proto.Clone(balance).(*m_balance.Balance))
		}

		sub.Balance <- dst
	}

	if len(pos) > 0 {
		for _, po := range pos {
			fulPos[fmt.Sprintf("%s-%d", strings.ToLower(po.Symbol.String()), po.Side)] = po
		}

		var dst []*m_position.Position
		for _, po := range fulPos {
			dst = append(dst, proto.Clone(po).(*m_position.Position))
		}

		sub.Position <- dst
	}
}

func (p *Perpetual) loginAfter(c *websocket.Conn) error {
	// 订阅余额和持仓
	data := map[string]interface{}{
		"op": "subscribe",
		"args": []interface{}{
			map[string]interface{}{
				"channel": "balance_and_position",
			},
		},
	}

	b, _ := util.JSONMarshal(data)
	if err := p.send(c, b); err != nil {
		return err
	}

	swaps := make([]interface{}, 1)
	swaps[0] = map[string]interface{}{
		"channel":  "orders",
		"instType": "SWAP",
	}

	data = map[string]interface{}{
		"op":   "subscribe",
		"args": swaps,
	}

	b, _ = util.JSONMarshal(data)
	return p.send(c, b)
}

func (p *Perpetual) login(apiKey, secret, passphrase string, c *websocket.Conn) error {
	var (
		timestamp = util.ToString(time.Now().Unix())
		sign      = util.HmacSha256Base64(fmt.Sprintf("%sGET/users/self/verify", timestamp), secret)
	)

	data := map[string]interface{}{
		"op": "login",
		"args": []interface{}{
			map[string]interface{}{
				"apiKey":     apiKey,
				"passphrase": passphrase,
				"timestamp":  timestamp,
				"sign":       sign,
			},
		},
	}

	d, _ := util.JSONMarshal(data)
	return p.send(c, d)
}

func (p *Perpetual) healthCheck(c *websocket.Conn, pingTimer, pongTimer *time.Ticker, manualClose *int32, close chan bool) {
	defer func() {
		_ = c.Close()
	}()

	for {
		select {
		case <-pingTimer.C:
			if err := p.send(c, []byte("ping")); err != nil {
				log.Error("发送PING失败:", err)
				return
			}
		case <-pongTimer.C:
			// 未收到PONG,自动重连
			break
		case <-close:
			atomic.StoreInt32(manualClose, 1)
			return
		}
	}
}
