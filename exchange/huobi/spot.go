package huobi

import (
	"errors"
	"github.com/go-resty/resty/v2"
	"github.com/gorilla/websocket"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/comp_acc"
	"gitlab.com/msex/comp_acc/exchange/huobi/define/spot"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"google.golang.org/protobuf/proto"
	"net/url"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

func init() {
	account.Reg(&Spot{
		symbolIdx: make(map[string]*m_symbol.Symbol),
	})
}

type Spot struct {
	symbolIdx map[string]*m_symbol.Symbol
	counter   int64
	chClose   sync.Map

	mtxSend sync.Mutex
}

func (s *Spot) Ty() m_common.Exchange {
	return m_common.Exchange_EX_HuoBi
}

func (s *Spot) ExTy() m_common.ExchangeType {
	return m_common.ExchangeType_ET_Spot
}

func (s *Spot) Start() error {
	rsp, err := resty.New().R().Get("https://api.huobi.pro/v1/common/symbols")
	if err != nil {
		log.Error("获取交易规则失败: ", err)
		return err
	}

	// 统计所有Symbol
	model := new(spot.ExchangeInfo)
	err = util.JSONUnmarshal(rsp.Body(), model)
	if err != nil {
		log.Error("解析交易规则失败: ", err)
		return err
	}

	s.symbolIdx = model.ToSymbolIdx()
	return nil
}

func (s *Spot) Sub(apiKey, secret, _ string) (*account.Subscribed, error) {
	sub := &account.Subscribed{
		Id:       atomic.AddInt64(&s.counter, 1),
		Position: make(chan []*m_position.Position, 1024),
		Balance:  make(chan []*m_balance.Balance, 1024),
		Order:    make(chan *m_order.Order, 1024),
		Leverage: make(chan []*m_setting.Leverage, 1024),
		Error:    make(chan error, 1),
	}

	chClose := make(chan bool, 1)
	s.chClose.Store(sub.Id, chClose)

	go s.sub(apiKey, secret, sub, chClose)
	return sub, nil
}

func (s *Spot) UnSub(subId int64) {
	ch, ok := s.chClose.LoadAndDelete(subId)
	if !ok {
		return
	}

	ch.(chan bool) <- true
}

func (s *Spot) ChangeSetting(string, string, string, []*m_setting.Leverage) error {
	return nil
}

func (s *Spot) sub(apiKey, secret string, sub *account.Subscribed, close chan bool) {
	baseUrl := "wss://api.huobi.pro/ws/v2"
	// 自动重连
	for {
		c, _, err := websocket.DefaultDialer.Dial(baseUrl, nil)
		if err != nil {
			log.Warn("连接ws流失败:", err)
			<-time.After(time.Second * 5)
			continue
		}

		// 人工关闭标记
		var manualClose int32
		go func() {
			<-close
			atomic.StoreInt32(&manualClose, 1)
			_ = c.Close()
		}()

		// 登录
		go func() {
			err = s.login(apiKey, secret, c)
			if err != nil {
				sub.Error <- err
			}
		}()

		head := new(spot.StreamHead)
		fulBal := make(map[string]*m_balance.Balance)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Warn("读取消息失败:", err)
				break
			}

			err = util.JSONUnmarshal(message, head)
			if err != nil {
				log.Warn("解析消息失败:", err)
				continue
			}

			switch head.Action {
			case "ping":
				err = s.sendMap(c, map[string]interface{}{
					"action": "pong",
					"data": map[string]interface{}{
						"ts": head.Data["ts"],
					},
				})

				if err != nil {
					log.Warn("发送pong失败:", err)
				}
			case "req":
				switch head.Ch {
				case "auth":
					if head.Code != 200 {
						sub.Error <- errors.New("鉴权失败")
						return
					}

					err = s.loginAfter(c)
					if err != nil {
						sub.Error <- err
					}
				}
			case "push":
				switch {
				case strings.HasPrefix(head.Ch, "accounts.update"):
					s.onUpdateBal(head.Data, fulBal, sub)
				case strings.HasPrefix(head.Ch, "trade.clearing#"):
					s.onUpdateOrder(head.Data, sub)
				}
			}
		}

		if atomic.LoadInt32(&manualClose) != 0 {
			return
		}
	}
}

func (s *Spot) pushBal(fulBal map[string]*m_balance.Balance, sub *account.Subscribed) {
	arr := make([]*m_balance.Balance, len(fulBal))
	var idx int
	for _, balance := range fulBal {
		arr[idx] = proto.Clone(balance).(*m_balance.Balance)
		idx++
	}

	sub.Balance <- arr
}

func (s *Spot) onUpdateBal(data map[string]interface{}, fulBal map[string]*m_balance.Balance, sub *account.Subscribed) {
	sym := strings.ToLower(util.ToString(data["currency"]))
	b := util.ToFloat64(data["balance"])
	fulBal[sym] = &m_balance.Balance{
		Symbol: &m_symbol.Symbol{
			Base: sym,
		},
		Balance: b,
	}

	s.pushBal(fulBal, sub)
}

func (s *Spot) login(apiKey, secret string, c *websocket.Conn) error {
	timestamp := time.Now().UTC().Format("2006-01-02T15:04:05")
	params := url.Values{}
	params.Add("accessKey", apiKey)
	params.Add("signatureMethod", "HmacSHA256")
	params.Add("signatureVersion", "2.1")
	params.Add("timestamp", timestamp)
	parameters := params.Encode()

	var sb strings.Builder
	sb.WriteString("GET")
	sb.WriteString("\n")
	sb.WriteString("api.huobi.pro")
	sb.WriteString("\n")
	sb.WriteString("/ws/v2")
	sb.WriteString("\n")
	sb.WriteString(parameters)

	signature := util.HmacSha256Base64(sb.String(), secret)

	data := map[string]interface{}{
		"action": "req",
		"ch":     "auth",
		"params": map[string]interface{}{
			"authType":         "api",
			"accessKey":        apiKey,
			"signatureMethod":  "HmacSHA256",
			"signatureVersion": "2.1",
			"timestamp":        timestamp,
			"signature":        signature,
		},
	}

	d, _ := util.JSONMarshal(data)
	return s.send(c, d)
}

func (s *Spot) sendMap(conn *websocket.Conn, m map[string]interface{}) error {
	data, _ := util.JSONMarshal(m)

	s.mtxSend.Lock()
	defer s.mtxSend.Unlock()

	return conn.WriteMessage(websocket.TextMessage, data)
}

func (s *Spot) send(conn *websocket.Conn, data []byte) error {
	s.mtxSend.Lock()
	defer s.mtxSend.Unlock()

	return conn.WriteMessage(websocket.TextMessage, data)
}

func (s *Spot) loginAfter(c *websocket.Conn) error {
	// 订阅余额和订单
	data := map[string]interface{}{
		"action": "sub",
		"ch":     "trade.clearing#*",
	}

	if err := s.sendMap(c, data); err != nil {
		return err
	}

	// 只订阅余额变动, 可用余额变动不管
	// Note: 火币现货账户与合约账户发生划转引起的变动不会推送!
	data = map[string]interface{}{
		"action": "sub",
		"ch":     "accounts.update",
	}

	return s.sendMap(c, data)
}

func (s *Spot) onUpdateOrder(data map[string]interface{}, sub *account.Subscribed) {
	switch util.ToString(data["eventType"]) {
	//case "creation":
	//	s.onUpdateCreationOrder(data, sub)
	case "trade":
		s.onUpdateTradeOrder(data, sub)
	}
}

func (s *Spot) onUpdateCreationOrder(data map[string]interface{}, sub *account.Subscribed) {
	stream := new(spot.StreamCreationOrder)
	d, err := util.JSONMarshal(data)
	if err != nil {
		sub.Error <- err
	}

	err = util.JSONUnmarshal(d, stream)
	if err != nil {
		sub.Error <- err
	}

	ord := stream.ToPB(s.symbolIdx)
	if ord == nil {
		return
	}

	sub.Order <- ord
}

func (s *Spot) onUpdateTradeOrder(data map[string]interface{}, sub *account.Subscribed) {
	// TODO: 区分新单，成交单，撤销单
	stream := new(spot.StreamTradeOrder)
	d, err := util.JSONMarshal(data)
	if err != nil {
		sub.Error <- err
	}

	err = util.JSONUnmarshal(d, stream)
	if err != nil {
		sub.Error <- err
	}

	ord := stream.ToPB(s.symbolIdx)
	if ord == nil {
		return
	}

	sub.Order <- ord
}
