package huobi

import (
	"bytes"
	"compress/gzip"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/comp_acc"
	"gitlab.com/msex/comp_acc/exchange/huobi/define/perpetual"
	"gitlab.com/msex/define"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"google.golang.org/protobuf/proto"
	"io/ioutil"
	"net/url"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

func init() {
	account.Reg(&Perpetual{
		symbolIdx: make(map[string]*m_symbol.Symbol),
	})
}

type Perpetual struct {
	symbolIdx  map[string]*m_symbol.Symbol
	symbolSize map[string]float64
	counter    int64
	chClose    sync.Map

	mtxSend sync.Mutex
}

func (s *Perpetual) Ty() m_common.Exchange {
	return m_common.Exchange_EX_HuoBi
}

func (s *Perpetual) ExTy() m_common.ExchangeType {
	return m_common.ExchangeType_ET_Perpetual
}

func (s *Perpetual) Start() (err error) {
	s.symbolIdx, s.symbolSize, err = perpetual.SymbolIndexAndContractSize()
	return
}

func (s *Perpetual) Sub(apiKey, secret, _ string) (*account.Subscribed, error) {
	sub := &account.Subscribed{
		Id:       atomic.AddInt64(&s.counter, 1),
		Position: make(chan []*m_position.Position, 1024),
		Balance:  make(chan []*m_balance.Balance, 1024),
		Order:    make(chan *m_order.Order, 1024),
		Leverage: make(chan []*m_setting.Leverage, 1024),
		Error:    make(chan error, 1),
	}

	chClose := make(chan bool, 1)
	s.chClose.Store(sub.Id, chClose)

	go s.sub(apiKey, secret, sub, chClose)
	return sub, nil
}

func (s *Perpetual) UnSub(subId int64) {
	ch, ok := s.chClose.LoadAndDelete(subId)
	if !ok {
		return
	}

	ch.(chan bool) <- true
}

func (s *Perpetual) ChangeSetting(string, string, string, []*m_setting.Leverage) error {
	// 火币杠杆3秒一次且需要同意协议,失败率高,请手动设置杠杆
	return define.ErrNotSupport
}

func (s *Perpetual) sub(apiKey, secret string, sub *account.Subscribed, close chan bool) {
	//accountId, err := spot.RestfulAllAccId(apiKey, secret)
	//if err != nil {
	//	sub.Error <- err
	//	return
	//}

	baseUrl := "wss://api.hbdm.com/linear-swap-notification"

	// 自动重连
	for {
		c, _, err := websocket.DefaultDialer.Dial(baseUrl, nil)
		if err != nil {
			log.Warn("连接ws流失败:", err)
			<-time.After(time.Second * 5)
			continue
		}

		// 人工关闭标记
		var manualClose int32
		go func() {
			<-close
			atomic.StoreInt32(&manualClose, 1)
			_ = c.Close()
		}()

		// 登录
		go func() {
			err = s.login(apiKey, secret, c)
			if err != nil {
				sub.Error <- err
			}
		}()

		head := new(perpetual.StreamHead)
		fulBal := make(map[string]*m_balance.Balance)
		fulPos := make(map[string]*m_position.Position)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Warn("读取消息失败:", err)
				break
			}

			r, err := gzip.NewReader(bytes.NewBuffer(message))
			if err != nil {
				log.Error("解压失败:", err)
				continue
			}

			data, err := ioutil.ReadAll(r)
			if err != nil {
				log.Error("解压失败:", err)
				continue
			}

			err = util.JSONUnmarshal(data, head)
			if err != nil {
				log.Warn("解析消息失败:", err)
				continue
			}

			switch head.Op {
			case "ping":
				err = s.sendMap(c, map[string]interface{}{
					"op": "pong",
					"ts": head.Ts,
				})

				if err != nil {
					log.Warn("发送pong失败:", err)
				}
			case "auth":
				if head.ErrCode != 0 {
					sub.Error <- errors.New("鉴权失败")
					return
				}

				err = s.loginAfter(c)
				if err != nil {
					sub.Error <- err
				}
			case "notify":
				switch {
				case strings.HasPrefix(head.Topic, "orders_cross"):
					s.onUpdateOrder(data, sub)
				case strings.HasPrefix(head.Topic, "accounts_cross"):
					s.onUpdateBal(data, fulBal, sub)
				case strings.HasPrefix(head.Topic, "positions_cross"):
					s.onUpdatePos(data, fulPos, sub)
				}
			}
		}

		if atomic.LoadInt32(&manualClose) != 0 {
			return
		}
	}
}

func (s *Perpetual) pushBal(fulBal map[string]*m_balance.Balance, sub *account.Subscribed) {
	arr := make([]*m_balance.Balance, len(fulBal))
	var idx int
	for _, balance := range fulBal {
		arr[idx] = proto.Clone(balance).(*m_balance.Balance)
		idx++
	}

	sub.Balance <- arr
}

func (s *Perpetual) pushPos(fulPos map[string]*m_position.Position, sub *account.Subscribed) {
	arr := make([]*m_position.Position, len(fulPos))
	var idx int
	for _, pos := range fulPos {
		arr[idx] = proto.Clone(pos).(*m_position.Position)
		idx++
	}

	sub.Position <- arr
}

func (s *Perpetual) login(apiKey, secret string, c *websocket.Conn) error {
	timestamp := time.Now().UTC().Format("2006-01-02T15:04:05")
	params := url.Values{}
	params.Add("AccessKeyId", apiKey)
	params.Add("SignatureMethod", "HmacSHA256")
	params.Add("SignatureVersion", "2")
	params.Add("Timestamp", timestamp)
	parameters := params.Encode()

	var sb strings.Builder
	sb.WriteString("GET")
	sb.WriteString("\n")
	sb.WriteString("api.hbdm.com")
	sb.WriteString("\n")
	sb.WriteString("/linear-swap-notification")
	sb.WriteString("\n")
	sb.WriteString(parameters)

	signature := util.HmacSha256Base64(sb.String(), secret)

	data := map[string]interface{}{
		"op":               "auth",
		"type":             "api",
		"AccessKeyId":      apiKey,
		"SignatureMethod":  "HmacSHA256",
		"SignatureVersion": "2",
		"Timestamp":        timestamp,
		"Signature":        signature,
	}

	d, _ := util.JSONMarshal(data)
	return s.send(c, d)
}

func (s *Perpetual) sendMap(conn *websocket.Conn, m map[string]interface{}) error {
	data, _ := util.JSONMarshal(m)

	s.mtxSend.Lock()
	defer s.mtxSend.Unlock()

	return conn.WriteMessage(websocket.TextMessage, data)
}

func (s *Perpetual) send(conn *websocket.Conn, data []byte) error {
	s.mtxSend.Lock()
	defer s.mtxSend.Unlock()

	return conn.WriteMessage(websocket.TextMessage, data)
}

func (s *Perpetual) loginAfter(c *websocket.Conn) error {
	// 订阅订单
	data := map[string]interface{}{
		"op":    "sub",
		"topic": "orders_cross.*",
	}

	if err := s.sendMap(c, data); err != nil {
		return err
	}

	// 订阅余额
	data = map[string]interface{}{
		"op": "sub",
		// TODO: 支持币本位
		"topic": "accounts_cross.USDT",
	}

	if err := s.sendMap(c, data); err != nil {
		return err
	}

	// 订阅持仓
	data = map[string]interface{}{
		"op":    "sub",
		"topic": "positions_cross.*",
	}

	return s.sendMap(c, data)
}

func (s *Perpetual) onUpdateOrder(data []byte, sub *account.Subscribed) {
	order, err := perpetual.NewOrderFromStream(data, s.symbolIdx, s.symbolSize)
	if err != nil {
		sub.Error <- err
		return
	}

	sub.Order <- order
}

func (s *Perpetual) onUpdateBal(data []byte, fulBal map[string]*m_balance.Balance, sub *account.Subscribed) {
	bal, err := perpetual.NewBalFromStream(data)
	if err != nil {
		sub.Error <- err
		return
	}

	for _, ba := range bal {
		fulBal[ba.Symbol.String()] = ba
	}

	s.pushBal(fulBal, sub)
}

func (s *Perpetual) onUpdatePos(data []byte, fulPos map[string]*m_position.Position, sub *account.Subscribed) {
	pos, err := perpetual.NewPosFromStream(data, s.symbolIdx, s.symbolSize)
	if err != nil {
		sub.Error <- err
		return
	}

	for _, po := range pos {
		fulPos[fmt.Sprintf("%s-%d", po.Symbol.String(), po.Side)] = po
	}

	s.pushPos(fulPos, sub)
}
