package spot

import (
	"errors"
	"fmt"
	"github.com/go-resty/resty/v2"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"net/url"
	"strings"
	"time"
)

type eiSymbol struct {
	Symbol     string `json:"symbol"`
	BaseAsset  string `json:"base-currency"`
	QuoteAsset string `json:"quote-currency"`
}

type ExchangeInfo struct {
	Symbols []*eiSymbol `json:"data"`
}

func (e *ExchangeInfo) ToSymbolIdx() map[string]*m_symbol.Symbol {
	idx := make(map[string]*m_symbol.Symbol)
	for _, symbol := range e.Symbols {
		s := &m_symbol.Symbol{
			Base:  symbol.BaseAsset,
			Quote: symbol.QuoteAsset,
		}

		idx[strings.ToLower(symbol.Symbol)] = s
	}

	return idx
}

type restfulAccounts struct {
	Status string `json:"status"`
	Data   []*struct {
		Id   int64  `json:"id"`
		Type string `json:"type"`
	} `json:"data"`
}

func RestfulAllAccId(apiKey, secret string) (int64, error) {
	params := url.Values{}
	params.Add("AccessKeyId", apiKey)
	params.Add("SignatureMethod", "HmacSHA256")
	params.Add("SignatureVersion", "2")
	params.Add("Timestamp", time.Now().UTC().Format("2006-01-02T15:04:05"))
	parameters := params.Encode()

	var sb strings.Builder
	sb.WriteString("GET")
	sb.WriteString("\n")
	sb.WriteString("api.huobi.pro")
	sb.WriteString("\n")
	sb.WriteString("/v1/account/accounts")
	sb.WriteString("\n")
	sb.WriteString(parameters)

	signature := util.HmacSha256Base64(sb.String(), secret)
	dstUrl := fmt.Sprintf("https://api.huobi.pro/v1/account/accounts?%s&Signature=%s", parameters, url.QueryEscape(signature))
	rsp, err := resty.New().
		R().
		Get(dstUrl)
	if err != nil {
		return 0, err
	}

	ra := new(restfulAccounts)
	err = util.JSONUnmarshal(rsp.Body(), ra)
	if err != nil {
		return 0, err
	}

	if ra.Status != "ok" {
		return 0, errors.New("get account id failed")
	}

	for _, e := range ra.Data {
		switch e.Type {
		case "spot":
			return e.Id, nil
		}
	}

	return 0, errors.New("not found account id")
}

type restfulBal struct {
	Status string `json:"status"`
	Data   *struct {
		List []*struct {
			Base    string  `json:"currency"`
			Balance float64 `json:"balance,string"`
		} `json:"list"`
	} `json:"data"`
}

func RestfulAllBal(apiKey, secret string, accountId int64) (map[string]*m_balance.Balance, error) {
	params := url.Values{}
	params.Add("AccessKeyId", apiKey)
	params.Add("SignatureMethod", "HmacSHA256")
	params.Add("SignatureVersion", "2")
	params.Add("Timestamp", time.Now().UTC().Format("2006-01-02T15:04:05"))
	parameters := params.Encode()

	var sb strings.Builder
	sb.WriteString("GET")
	sb.WriteString("\n")
	sb.WriteString("api.huobi.pro")
	sb.WriteString("\n")
	sb.WriteString(fmt.Sprintf("/v1/account/accounts/%d/balance", accountId))
	sb.WriteString("\n")
	sb.WriteString(parameters)

	signature := util.HmacSha256Base64(sb.String(), secret)
	dstUrl := fmt.Sprintf("https://api.huobi.pro/v1/account/accounts/%d/balance?%s&Signature=%s", accountId, parameters, url.QueryEscape(signature))
	rsp, err := resty.New().R().Get(dstUrl)
	if err != nil {
		return nil, err
	}

	rb := new(restfulBal)
	err = util.JSONUnmarshal(rsp.Body(), rb)
	if err != nil {
		return nil, err
	}

	if rb.Status != "ok" {
		return nil, errors.New("get full balance failed")
	}

	m := make(map[string]*m_balance.Balance)
	for _, e := range rb.Data.List {
		if e.Balance == 0 {
			continue
		}

		b := strings.ToLower(e.Base)
		m[b] = &m_balance.Balance{
			Symbol: &m_symbol.Symbol{
				Base: b,
			},
			Balance: e.Balance,
		}
	}

	return m, nil
}
