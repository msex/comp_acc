package spot

import (
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"strings"
)

type Ping struct {
	Action string `json:"action"`
	Data   *struct {
		Ts int64 `json:"ts"`
	}
}

func (p *Ping) Reset() {
	p.Action = ""
	if p.Data != nil {
		p.Data.Ts = 0
	}
}

func (p *Ping) IsPing() bool {
	return p.Action == "ping"
}

func (p *Ping) ToPongBytes() []byte {
	p.Action = "pong"
	b, _ := util.JSONMarshal(p)
	return b
}

type StreamHead struct {
	Action string                 `json:"action"`
	Ch     string                 `json:"ch"`
	Code   int                    `json:"code"`
	Data   map[string]interface{} `json:"data"`
}

type StreamCreationOrder struct {
	Id         string  `json:"clientOrderId"`
	Symbol     string  `json:"symbol"`
	Price      float64 `json:"orderPrice,string"`
	Quantity   string  `json:"orderSize"`  // non-market-buy
	TransPrice string  `json:"orderValue"` // only-market-buy
	Type       string  `json:"type"`
	Ts         int64   `json:"orderCreateTime"`
}

func typeAndSide(raw string) (ty m_order.OrderType, si m_order.OrderSide) {
	arr := strings.Split(raw, "-")
	if len(arr) != 2 {
		return
	}

	switch arr[0] {
	case "buy":
		si = m_order.OrderSide_OS_Buy
	case "sell":
		si = m_order.OrderSide_OS_Sell
	}

	switch arr[1] {
	case "market":
		ty = m_order.OrderType_OT_Market
	case "limit":
		ty = m_order.OrderType_OT_Limit
	}

	return
}

func (s *StreamCreationOrder) ToPB(symIdx map[string]*m_symbol.Symbol) *m_order.Order {
	var (
		oTy, oSi   = typeAndSide(s.Type)
		sym        = symIdx[strings.ToLower(s.Symbol)]
		quan       float64
		transPrice float64
	)

	if oTy == m_order.OrderType_OT_None || oSi == m_order.OrderSide_OS_None {
		return nil
	}

	if oTy == m_order.OrderType_OT_Market &&
		oSi == m_order.OrderSide_OS_Buy {
		transPrice = util.ToFloat64(s.TransPrice)
	} else {
		quan = util.ToFloat64(s.Quantity)
	}

	return &m_order.Order{
		Id:           s.Id,
		Ex:           m_common.Exchange_EX_HuoBi,
		ETy:          m_common.ExchangeType_ET_Spot,
		OTy:          oTy,
		Symbol:       sym,
		PositionSide: m_position.PositionSide_PS_Long,
		OrderSide:    oSi,
		Quantity:     quan,
		Price:        s.Price,
		TransPrice:   transPrice,
		Ts:           s.Ts,
	}
}

type StreamTradeOrder struct {
	Id             string  `json:"clientOrderId"`
	Symbol         string  `json:"symbol"`
	Type           string  `json:"orderType"`
	Price          string  `json:"orderPrice"`
	Quantity       string  `json:"orderSize"`
	OrderValue     string  `json:"orderValue"`
	LatestPrice    float64 `json:"tradePrice,string"`
	LatestQuantity float64 `json:"tradeVolume,string"`
	FeeSymbol      string  `json:"feeCurrency"`
	Fee            string  `json:"transactFee"`
	Status         string  `json:"orderStatus"`
	Ts             int64   `json:"tradeTime"`
}

func (s *StreamTradeOrder) ToPB(symIdx map[string]*m_symbol.Symbol) *m_order.Order {
	var (
		oTy, oSi       = typeAndSide(s.Type)
		sym            = symIdx[strings.ToLower(s.Symbol)]
		quan           float64
		transPrice     float64
		transExecPrice float64
		price          float64
	)

	// TODO: 给大傻逼火币市价现货测试
	if oTy == m_order.OrderType_OT_None || oSi == m_order.OrderSide_OS_None {
		return nil
	}

	if oTy != m_order.OrderType_OT_Market {
		price = util.ToFloat64(s.Price)
	}

	if oTy == m_order.OrderType_OT_Market && oSi == m_order.OrderSide_OS_Buy {
		transPrice = util.ToFloat64(s.OrderValue)
		transExecPrice = util.ToFloat64(s.LatestPrice) * util.ToFloat64(s.LatestQuantity)
	} else {
		quan = util.ToFloat64(s.Quantity)
	}

	return &m_order.Order{
		Id:             s.Id,
		Ex:             m_common.Exchange_EX_HuoBi,
		ETy:            m_common.ExchangeType_ET_Spot,
		OTy:            oTy,
		Symbol:         sym,
		PositionSide:   m_position.PositionSide_PS_Long,
		OrderSide:      oSi,
		Quantity:       quan,
		LatestQuantity: s.LatestQuantity,
		Price:          price,
		LatestPrice:    s.LatestPrice,
		TransPrice:     transPrice,
		TransExecPrice: transExecPrice,
		FeeSymbol:      strings.ToLower(s.FeeSymbol),
		Fee:            util.ToFloat64(s.Fee),
		Finished:       s.Status == "filled",
		Ts:             s.Ts,
	}
}
