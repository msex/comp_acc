package perpetual

import (
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/define"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"math"
	"strings"
)

type StreamHead struct {
	Op      string      `json:"op"`
	Topic   string      `json:"topic"`
	Ts      interface{} `json:"ts"`
	ErrCode int         `json:"err_code"`
}

type streamOrder struct {
	Id            int64   `json:"client_order_id,omitempty"`
	Ts            int64   `json:"ts"`
	ContractCode  string  `json:"contract_code"`
	OTy           string  `json:"order_price_type"`
	OSide         string  `json:"direction"`
	Offset        string  `json:"offset"`
	Quantity      float64 `json:"volume"`
	TotalQuantity float64 `json:"trade_volume"`
	Price         float64 `json:"price"`
	AvgPrice      float64 `json:"trade_avg_price"`
	Status        int     `json:"status"`
	Trade         []*struct {
		LatestPrice    float64 `json:"trade_price"`
		LatestQuantity float64 `json:"trade_volume"`
		FeeSymbol      string  `json:"fee_asset"`
		Fee            float64 `json:"trade_fee"`
	} `json:"trade"`
}

func NewOrderFromStream(data []byte, idx map[string]*m_symbol.Symbol, sz map[string]float64) (*m_order.Order, error) {
	// TODO: 区分新单，成交单，撤销单
	ord := new(streamOrder)
	err := util.JSONUnmarshal(data, ord)
	if err != nil {
		return nil, err
	}

	var (
		oTy            m_order.OrderType
		symbol         *m_symbol.Symbol
		positionSide   m_position.PositionSide
		orderSide      m_order.OrderSide
		mul            float64
		latestPrice    float64
		latestQuantity float64
		feeSymbol      string
		fee            float64
	)

	switch ord.OTy {
	case "limit":
		oTy = m_order.OrderType_OT_Limit
	case "opponent":
		oTy = m_order.OrderType_OT_Market
	default:
		return nil, define.ErrNotSupport
	}

	symbol = idx[strings.ToLower(ord.ContractCode)]
	switch {
	case ord.OSide == "buy" && ord.Offset == "open":
		positionSide = m_position.PositionSide_PS_Long
	case ord.OSide == "sell" && ord.Offset == "close":
		positionSide = m_position.PositionSide_PS_Long
	case ord.OSide == "buy" && ord.Offset == "close":
		positionSide = m_position.PositionSide_PS_Short
	case ord.OSide == "sell" && ord.Offset == "open":
		positionSide = m_position.PositionSide_PS_Short
	default:
		return nil, define.ErrNotSupport
	}

	switch ord.OSide {
	case "buy":
		orderSide = m_order.OrderSide_OS_Buy
	case "sell":
		orderSide = m_order.OrderSide_OS_Sell
	default:
		return nil, define.ErrNotSupport
	}

	mul = sz[strings.ToLower(ord.ContractCode)]

	if len(ord.Trade) > 0 {
		t := ord.Trade[len(ord.Trade)-1]
		latestPrice = t.LatestPrice
		latestQuantity = t.LatestQuantity
		feeSymbol = strings.ToLower(t.FeeSymbol)
		fee = math.Abs(t.Fee)
	}

	order := &m_order.Order{
		Ex:             m_common.Exchange_EX_HuoBi,
		ETy:            m_common.ExchangeType_ET_Perpetual,
		OTy:            oTy,
		Symbol:         symbol,
		PositionSide:   positionSide,
		OrderSide:      orderSide,
		Quantity:       ord.Quantity * mul,
		LatestQuantity: latestQuantity * mul,
		TotalQuantity:  ord.TotalQuantity * mul,
		Price:          ord.Price,
		LatestPrice:    latestPrice,
		AvgPrice:       ord.AvgPrice,
		FeeSymbol:      feeSymbol,
		Fee:            fee,
		Finished:       ord.Status == 6,
		Ts:             ord.Ts,
	}

	if ord.Id != 0 {
		order.Id = util.ToString(ord.Id)
	}

	return order, nil
}

type streamBal struct {
	Data []*struct {
		Symbol  string  `json:"margin_account"`
		Balance float64 `json:"margin_balance"`
	} `json:"data"`
}

func NewBalFromStream(data []byte) ([]*m_balance.Balance, error) {
	bal := new(streamBal)
	err := util.JSONUnmarshal(data, bal)
	if err != nil {
		return nil, err
	}

	arr := make([]*m_balance.Balance, len(bal.Data))
	for i, e := range bal.Data {
		arr[i] = &m_balance.Balance{
			Symbol: &m_symbol.Symbol{
				Base: strings.ToLower(e.Symbol),
			},
			Balance: e.Balance,
		}
	}

	return arr, nil
}

type streamPos struct {
	Event string `json:"event"`
	Data  []*struct {
		ContractCode string  `json:"contract_code"`
		Price        float64 `json:"cost_hold"`
		Quantity     float64 `json:"volume"`
		Direction    string  `json:"direction"`
		MarginMode   string  `json:"margin_mode"`
	} `json:"data"`
}

func NewPosFromStream(data []byte, idx map[string]*m_symbol.Symbol, sz map[string]float64) ([]*m_position.Position, error) {
	pos := new(streamPos)
	err := util.JSONUnmarshal(data, pos)
	if err != nil {
		return nil, err
	}

	var arr []*m_position.Position
	for _, e := range pos.Data {
		// TODO: 支持逐仓
		if e.MarginMode != "cross" {
			continue
		}

		if e.Quantity == 0 {
			continue
		}

		var (
			symbol *m_symbol.Symbol
			side   m_position.PositionSide
			mul    = float64(1)
		)

		symbol = idx[strings.ToLower(e.ContractCode)]
		switch e.Direction {
		case "buy":
			side = m_position.PositionSide_PS_Long
		case "sell":
			side = m_position.PositionSide_PS_Short
			// 做空用负数
			mul *= -1
		}

		if v, ok := sz[strings.ToLower(e.ContractCode)]; ok {
			mul *= v
		}

		arr = append(arr, &m_position.Position{
			Symbol:   symbol,
			Side:     side,
			Quantity: e.Quantity * mul,
			Price:    e.Price,
			Mt:       m_setting.MarginType_MT_CROSSED,
		})
	}

	return arr, nil
}
