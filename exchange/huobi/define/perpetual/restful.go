package perpetual

import (
	"github.com/go-resty/resty/v2"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"strings"
)

type eiSymbol struct {
	Symbol       string  `json:"symbol"`
	ContractCode string  `json:"contract_code"`
	ContractSize float64 `json:"contract_size"`
}

type exchangeInfo struct {
	Symbols []*eiSymbol `json:"data"`
}

func (e *exchangeInfo) toIdxAndSize() (map[string]*m_symbol.Symbol, map[string]float64, error) {
	idx := make(map[string]*m_symbol.Symbol)
	sz := make(map[string]float64)
	for _, symbol := range e.Symbols {
		var (
			base  string
			quote string
			cc    string
		)

		base = strings.ToLower(symbol.Symbol)
		quote = strings.ToLower(strings.Split(symbol.ContractCode, "-")[1])
		cc = strings.ToLower(symbol.ContractCode)
		idx[cc] = &m_symbol.Symbol{
			Base:  base,
			Quote: quote,
		}

		sz[cc] = symbol.ContractSize
	}

	return idx, sz, nil
}

// SymbolIndexAndContractSize 合约索引&每"张"面值
func SymbolIndexAndContractSize() (map[string]*m_symbol.Symbol, map[string]float64, error) {
	rsp, err := resty.New().
		R().
		Get("https://api.hbdm.com/linear-swap-api/v1/swap_contract_info")

	if err != nil {
		return nil, nil, err
	}

	ei := new(exchangeInfo)
	err = util.JSONUnmarshal(rsp.Body(), ei)
	if err != nil {
		return nil, nil, err
	}

	return ei.toIdxAndSize()
}
