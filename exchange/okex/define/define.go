package define

import (
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"strings"
)

type SymbolItem struct {
	Symbol            string  `json:"instId"`
	Ty                string  `json:"instType"`
	BaseP             string  `json:"ctValCcy"`
	QuoteP            string  `json:"settleCcy"`
	BaseS             string  `json:"baseCcy"`
	QuoteS            string  `json:"quoteCcy"`
	PricePrecision    float64 `json:"tickSz,string"`
	QuantityPrecision float64 `json:"lotSz,string"`
	CtVal             string  `json:"ctVal"`
}

func (s *SymbolItem) Base() string {
	switch s.Ty {
	case "SPOT":
		return strings.ToLower(s.BaseS)
	case "SWAP":
		return strings.ToLower(s.BaseP)
	}

	return ""
}

func (s *SymbolItem) Quote() string {
	switch s.Ty {
	case "SPOT":
		return strings.ToLower(s.QuoteS)
	case "SWAP":
		return strings.ToLower(s.QuoteP)
	}

	return ""
}

type SymbolInstrument struct {
	Data []*SymbolItem `json:"data"`
}

func (s *SymbolInstrument) ToSymbolIdx() map[string]*m_symbol.Symbol {
	m := make(map[string]*m_symbol.Symbol)
	for _, v := range s.Data {
		m[strings.ToLower(v.Symbol)] = &m_symbol.Symbol{
			Base:  v.Base(),
			Quote: v.Quote(),
		}
	}

	return m
}

func (s *SymbolInstrument) ToMap() map[string]*SymbolItem {
	m := make(map[string]*SymbolItem)
	for _, v := range s.Data {
		m[strings.ToLower(v.Symbol)] = v
	}

	return m
}
