package define

import (
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/define"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"math"
	"strings"
)

type balanceStream struct {
	Symbol string  `json:"ccy"`
	Amount float64 `json:"cashBal,string"`
}

type positionStream struct {
	Symbol       string  `json:"instId"`
	Ty           string  `json:"instType"`
	Amount       float64 `json:"pos,string"`
	Price        float64 `json:"avgPx,string"`
	PositionSide string  `json:"posSide"`
	MgnMode      string  `json:"mgnMode"`
}

type streamAccount struct {
	Account []struct {
		Balance  []*balanceStream  `json:"balData"`
		Position []*positionStream `json:"posData"`
		Tm       int64             `json:"pTime,string"`
	} `json:"data"`
}

func (a *streamAccount) toBalPB() []*m_balance.Balance {
	if len(a.Account) < 1 {
		return nil
	}

	var (
		sb []*m_balance.Balance
	)

	acc := a.Account[0]
	for _, b := range acc.Balance {
		sb = append(sb, &m_balance.Balance{
			Symbol: &m_symbol.Symbol{
				Base: strings.ToLower(b.Symbol),
			},
			Balance: b.Amount,
		})
	}

	return sb
}

func (a *streamAccount) toPosPB(symbolIdx map[string]*m_symbol.Symbol, symInst map[string]*SymbolItem) []*m_position.Position {
	if len(a.Account) < 1 {
		return nil
	}

	var (
		sp []*m_position.Position
	)

	acc := a.Account[0]
	for _, p := range acc.Position {
		// TODO: 支持逐仓
		if p.MgnMode == "isolated" {
			continue
		}

		symbol, ok := symbolIdx[strings.ToLower(p.Symbol)]
		if !ok {
			log.Error(define.ErrUnknownSymbol)
			continue
		}

		var (
			mul          float64
			positionSide m_position.PositionSide
			mt           m_setting.MarginType
		)

		switch p.PositionSide {
		case "net", "long":
			positionSide = m_position.PositionSide_PS_Long
			mul = 1
		case "short":
			positionSide = m_position.PositionSide_PS_Short
			// 做空用负数量
			mul = -1
		}

		switch p.MgnMode {
		case "isolated":
			mt = m_setting.MarginType_MT_ISOLATED
		case "cross":
			mt = m_setting.MarginType_MT_CROSSED
		}

		if p.Ty == "SWAP" {
			if inst, ok := symInst[strings.ToLower(p.Symbol)]; ok {
				v := util.ToFloat64(inst.CtVal)
				if v != 0 {
					mul *= v
				}
			}
		}

		sp = append(sp, &m_position.Position{
			Symbol:   symbol,
			Side:     positionSide,
			Quantity: p.Amount * mul,
			Price:    p.Price,
			Mt:       mt,
		})
	}
	return sp
}

func (a *streamAccount) toBalAndPosPB(symbolIdx map[string]*m_symbol.Symbol, symInst map[string]*SymbolItem) ([]*m_balance.Balance, []*m_position.Position) {
	return a.toBalPB(), a.toPosPB(symbolIdx, symInst)
}

func StreamParseBalAndPos(r []byte, symInx map[string]*m_symbol.Symbol, symInst map[string]*SymbolItem) ([]*m_balance.Balance, []*m_position.Position) {
	a := new(streamAccount)
	err := util.JSONUnmarshal(r, a)
	if err != nil {
		log.Warn("解析消息失败:", err)
		return nil, nil
	}

	return a.toBalAndPosPB(symInx, symInst)
}

func StreamParseBal(r []byte) []*m_balance.Balance {
	a := new(streamAccount)
	err := util.JSONUnmarshal(r, a)
	if err != nil {
		log.Warn("解析消息失败:", err)
		return nil
	}

	return a.toBalPB()
}

type orderStreamItem struct {
	ETy            string `json:"instType"`
	Id             string `json:"clOrdId"`
	Symbol         string `json:"instId"`
	Side           string `json:"side"`
	Ty             string `json:"ordType"`
	PositionSide   string `json:"posSide"`
	Quantity       string `json:"sz,omitempty"`
	LatestQuantity string `json:"fillSz,omitempty"`
	TotalQuantity  string `json:"accFillSz,omitempty"`
	Price          string `json:"px,omitempty"`
	LatestPrice    string `json:"fillPx,omitempty"`
	AvgPrice       string `json:"avgPx,omitempty"`
	TgtCcy         string `json:"tgtCcy"`
	State          string `json:"state"`
	FeeSymbol      string `json:"fillFeeCcy"`
	Fee            string `json:"fillFee"`
	Tm             int64  `json:"uTime,string"`
}

type orderStream struct {
	Order []*orderStreamItem `json:"data"`
}

func (o *orderStream) toPB(symInx map[string]*m_symbol.Symbol, symInst map[string]*SymbolItem) *m_order.Order {
	if len(o.Order) < 1 {
		return nil
	}

	if len(o.Order) > 1 {
		log.Warn("订单推送大于1:", o.Order)
	}

	var (
		eTy          m_common.ExchangeType
		ty           m_order.OrderType
		side         m_order.OrderSide
		positionSide m_position.PositionSide
		sym          *m_symbol.Symbol
		mul          = float64(1)
	)

	order := o.Order[0]
	switch order.ETy {
	case "SPOT":
		eTy = m_common.ExchangeType_ET_Spot
	case "SWAP":
		eTy = m_common.ExchangeType_ET_Perpetual
	default:
		return nil
	}

	switch order.Ty {
	case "market":
		ty = m_order.OrderType_OT_Market
	case "limit":
		ty = m_order.OrderType_OT_Limit
	default:
		return nil
	}

	switch order.Side {
	case "buy":
		side = m_order.OrderSide_OS_Buy
	case "sell":
		side = m_order.OrderSide_OS_Sell
	}

	switch order.PositionSide {
	case "net", "long":
		positionSide = m_position.PositionSide_PS_Long
	case "short":
		positionSide = m_position.PositionSide_PS_Short
	}

	sym = symInx[strings.ToLower(order.Symbol)]

	// 永续加工
	if eTy == m_common.ExchangeType_ET_Perpetual {
		// 单位张换为base数量
		if inst, ok := symInst[strings.ToLower(order.Symbol)]; ok {
			v := util.ToFloat64(inst.CtVal)
			if v != 0 {
				mul *= v
			}
		}
	}

	ord := &m_order.Order{
		Id:             order.Id,
		Ex:             m_common.Exchange_EX_OkEx,
		ETy:            eTy,
		OTy:            ty,
		Symbol:         sym,
		PositionSide:   positionSide,
		OrderSide:      side,
		Price:          util.ToFloat64(order.Price),
		LatestPrice:    util.ToFloat64(order.LatestPrice),
		AvgPrice:       util.ToFloat64(order.AvgPrice),
		LatestQuantity: util.ToFloat64(order.LatestQuantity) * mul,
		TotalQuantity:  util.ToFloat64(order.TotalQuantity) * mul,
		FeeSymbol:      strings.ToLower(order.FeeSymbol),
		Fee:            math.Abs(util.ToFloat64(order.Fee)),
		Finished:       order.State == "filled",
		Ts:             order.Tm,
	}

	if eTy == m_common.ExchangeType_ET_Spot &&
		ty == m_order.OrderType_OT_Market &&
		order.TgtCcy == "quote_ccy" {
		// 现货市价买单支持买多少quote的货
		ord.TransPrice = util.ToFloat64(order.Quantity) * mul
		if order.State == "filled" {
			// ok 无法跟踪订单进度, 待完全成交手动设置
			ord.TransExecPrice = ord.TransPrice
		}
	} else {
		ord.Quantity = util.ToFloat64(order.Quantity) * mul
	}

	return ord
}

func StreamParseOrder(r []byte, symInx map[string]*m_symbol.Symbol, symInst map[string]*SymbolItem) *m_order.Order {
	// TODO: 区分新单，成交单，撤销单
	o := new(orderStream)
	err := util.JSONUnmarshal(r, o)
	if err != nil {
		return nil
	}

	return o.toPB(symInx, symInst)
}
