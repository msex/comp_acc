package define

import (
	"errors"
	"fmt"
	"github.com/go-resty/resty/v2"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"strings"
	"time"
)

type restfulLeverage struct {
	InstId  string `json:"instId"`
	Lever   string `json:"lever"`
	MgMode  string `json:"mgnMode"`
	PosSide string `json:"posSide"`
}

type restfulLeverageRsp struct {
	Code string `json:"code"`
	Msg  string `json:"msg"`
}

func restfulLeverageBody(symbol *m_symbol.Symbol, v int, ps m_position.PositionSide) []byte {
	var psStr string
	switch ps {
	case m_position.PositionSide_PS_Long:
		psStr = "long"
	case m_position.PositionSide_PS_Short:
		psStr = "short"
	}

	// TODO: 支持逐仓
	r := &restfulLeverage{
		InstId:  fmt.Sprintf("%s-%s-SWAP", strings.ToUpper(symbol.Base), strings.ToUpper(symbol.Quote)),
		Lever:   util.ToString(v),
		MgMode:  "cross",
		PosSide: psStr,
	}

	b, _ := util.JSONMarshal(r)
	return b
}

func RestfulChangeLeverage(apiKey, secret, passphrase string, symbol *m_symbol.Symbol, v int) error {
	err := restfulChangeLeverageWithPosSide(apiKey, secret, passphrase, symbol, v, m_position.PositionSide_PS_Long)
	if err != nil {
		return err
	}

	return restfulChangeLeverageWithPosSide(apiKey, secret, passphrase, symbol, v, m_position.PositionSide_PS_Short)
}

func restfulChangeLeverageWithPosSide(apiKey, secret, passphrase string, symbol *m_symbol.Symbol, v int, ps m_position.PositionSide) error {
	var (
		ts   = time.Now().UTC().Format("2006-01-02T15:04:05.000Z")
		body = restfulLeverageBody(symbol, v, ps)
		sign string
	)

	sign = util.HmacSha256Base64(fmt.Sprintf("%sPOST/api/v5/account/set-leverage%s", ts, string(body)), secret)

	cli := resty.New()
	rsp, err := cli.R().
		SetHeader("accept", "application/json").
		SetHeader("Content-Type", "application/json").
		SetHeader("OK-ACCESS-KEY", apiKey).
		SetHeader("OK-ACCESS-SIGN", sign).
		SetHeader("OK-ACCESS-TIMESTAMP", ts).
		SetHeader("OK-ACCESS-PASSPHRASE", passphrase).
		SetBody(body).
		Post("https://www.okex.com/api/v5/account/set-leverage")
	if err != nil {
		return err
	}

	r := new(restfulLeverageRsp)
	err = util.JSONUnmarshal(rsp.Body(), r)
	if err != nil {
		return err
	}

	if r.Code != "0" {
		return errors.New(r.Msg)
	}

	return nil
}
