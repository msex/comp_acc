package okex

import (
	"errors"
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/gorilla/websocket"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/comp_acc"
	"gitlab.com/msex/comp_acc/exchange/okex/define"
	define2 "gitlab.com/msex/define"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_symbol"
	"google.golang.org/protobuf/proto"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

func init() {
	account.Reg(&Spot{
		symbolIdx:  make(map[string]*m_symbol.Symbol),
		symbolInst: make(map[string]*define.SymbolItem),
	})
}

type Spot struct {
	symbolIdx  map[string]*m_symbol.Symbol
	symbolInst map[string]*define.SymbolItem
	counter    int64
	chClose    sync.Map
	mtxSend    sync.Mutex
}

func (s *Spot) Ty() m_common.Exchange {
	return m_common.Exchange_EX_OkEx
}

func (s *Spot) ExTy() m_common.ExchangeType {
	return m_common.ExchangeType_ET_Spot
}

func (s *Spot) Start() error {
	cli := resty.New()
	rsp, err := cli.R().
		Get("https://www.okex.com/api/v5/public/instruments?instType=SPOT")
	if err != nil {
		log.Error("获取交易规则失败: ", err)
		return err
	}

	// 统计所有Symbol
	model := new(define.SymbolInstrument)
	err = util.JSONUnmarshal(rsp.Body(), model)
	if err != nil {
		log.Error("解析交易规则失败: ", err)
		return err
	}

	s.symbolIdx = model.ToSymbolIdx()
	s.symbolInst = model.ToMap()
	return nil
}

func (s *Spot) Sub(apiKey, secret, extended string) (*account.Subscribed, error) {
	sub := &account.Subscribed{
		Id:       atomic.AddInt64(&s.counter, 1),
		Position: make(chan []*m_position.Position, 1024),
		Balance:  make(chan []*m_balance.Balance, 1024),
		Order:    make(chan *m_order.Order, 1024),
		Leverage: make(chan []*m_setting.Leverage, 1024),
		Error:    make(chan error, 1),
	}

	chClose := make(chan bool, 1)
	s.chClose.Store(sub.Id, chClose)

	go s.sub(apiKey, secret, extended, sub, chClose)
	return sub, nil
}

func (s *Spot) UnSub(subId int64) {
	ch, ok := s.chClose.LoadAndDelete(subId)
	if !ok {
		return
	}

	ch.(chan bool) <- true
}

func (s *Spot) ChangeSetting(string, string, string, []*m_setting.Leverage) error {
	return define2.ErrNotSupport
}

func (s *Spot) send(conn *websocket.Conn, data []byte) error {
	s.mtxSend.Lock()
	defer s.mtxSend.Unlock()

	return conn.WriteMessage(websocket.TextMessage, data)
}

func (s *Spot) sub(apiKey, secret, passphrase string, sub *account.Subscribed, close chan bool) {
	heartBeat := time.Second * 30
	pingTimer, pongTimer := time.NewTicker(time.Second*7), time.NewTicker(heartBeat)
	defer func() {
		pingTimer.Stop()
		pongTimer.Stop()
	}()

	baseUrl := "wss://ws.okex.com:8443/ws/v5/private"

	// 自动重连
	for {
		c, _, err := websocket.DefaultDialer.Dial(baseUrl, nil)
		if err != nil {
			log.Warn("连接ws流失败:", err)
			<-time.After(time.Second * 5)
			continue
		}

		// 人工关闭标记
		var manualClose int32
		// 定期发PING, 关闭监听
		go s.healthCheck(c, pingTimer, pongTimer, &manualClose, close)

		// 登录
		go func() {
			err = s.login(apiKey, secret, passphrase, c)
			if err != nil {
				sub.Error <- err
			}
		}()

		fulBal := make(map[string]*m_balance.Balance)
		//fulPos := make(map[string]*m_position.Position)

		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Warn("读取消息失败:", err)
				break
			}

			if string(message) == "pong" {
				pongTimer.Reset(heartBeat)
				continue
			}

			ev := make(map[string]interface{})

			// 先分析事件类型
			err = util.JSONUnmarshal(message, &ev)
			if err != nil {
				log.Warn("解析消息失败:", err)
				continue
			}

			// 事件
			switch util.ToString(ev["event"]) {
			case "login":
				err = s.loginAfter(c)
				if err != nil {
					sub.Error <- err
				}
			case "error":
				sub.Error <- errors.New(util.ToString(ev["msg"]))
			}

			if _, ok := ev["arg"]; !ok {
				continue
			}

			// 推送流
			channel := util.ToStringMap(ev["arg"])["channel"]
			switch util.ToString(channel) {
			case "balance_and_position":
				s.onUpdateBal(message, sub, fulBal)
			case "orders":
				s.onUpdateOrder(message, sub)
			}
		}

		if atomic.LoadInt32(&manualClose) != 0 {
			return
		}
	}
}

func (s *Spot) onUpdateOrder(message []byte, sub *account.Subscribed) bool {
	o := define.StreamParseOrder(message, s.symbolIdx, s.symbolInst)
	if o == nil || o.ETy != m_common.ExchangeType_ET_Spot {
		return true
	}

	sub.Order <- o
	return false
}

func (s *Spot) onUpdateBal(message []byte, sub *account.Subscribed, fulBal map[string]*m_balance.Balance) {
	bal := define.StreamParseBal(message)
	if len(bal) == 0 {
		return
	}

	for _, ba := range bal {
		fulBal[strings.ToLower(ba.Symbol.String())] = ba
	}

	var dst []*m_balance.Balance
	for _, balance := range fulBal {
		dst = append(dst, proto.Clone(balance).(*m_balance.Balance))
	}

	sub.Balance <- dst
}

func (s *Spot) loginAfter(c *websocket.Conn) error {
	// 订阅余额
	data := map[string]interface{}{
		"op": "subscribe",
		"args": []interface{}{
			map[string]interface{}{
				"channel": "balance_and_position",
			},
		},
	}

	b, _ := util.JSONMarshal(data)
	if err := s.send(c, b); err != nil {
		return err
	}

	spots := make([]interface{}, 1)
	spots[0] = map[string]interface{}{
		"channel":  "orders",
		"instType": "SPOT",
	}

	data = map[string]interface{}{
		"op":   "subscribe",
		"args": spots,
	}

	b, _ = util.JSONMarshal(data)
	return s.send(c, b)
}

func (s *Spot) login(apiKey, secret, passphrase string, c *websocket.Conn) error {
	var (
		timestamp = util.ToString(time.Now().Unix())
		sign      = util.HmacSha256Base64(fmt.Sprintf("%sGET/users/self/verify", timestamp), secret)
	)

	data := map[string]interface{}{
		"op": "login",
		"args": []interface{}{
			map[string]interface{}{
				"apiKey":     apiKey,
				"passphrase": passphrase,
				"timestamp":  timestamp,
				"sign":       sign,
			},
		},
	}

	d, _ := util.JSONMarshal(data)
	return s.send(c, d)
}

func (s *Spot) healthCheck(c *websocket.Conn, pingTimer, pongTimer *time.Ticker, manualClose *int32, close chan bool) {
	defer func() {
		_ = c.Close()
	}()

	for {
		select {
		case <-pingTimer.C:
			if err := s.send(c, []byte("ping")); err != nil {
				log.Error("发送PING失败:", err)
				return
			}
		case <-pongTimer.C:
			// 未收到PONG,自动重连
			break
		case <-close:
			atomic.StoreInt32(manualClose, 1)
			return
		}
	}
}
