package account

import (
	"fmt"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/orm"
	conf "gitlab.com/flex_comp/remote_conf"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/define"
	"gitlab.com/msex/model_db/model/db_acc"
	"gitlab.com/msex/model_db/model/db_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
)

var (
	ins *Account
)

func init() {
	ins = &Account{
		exchanges: make(map[string]Exchange),
	}

	_ = comp.RegComp(ins)
}

type Account struct {
	exchanges map[string]Exchange
}

func (a *Account) Init(map[string]interface{}, ...interface{}) (err error) {
	for _, exchange := range a.exchanges {
		err = exchange.Start()
		if err != nil {
			return err
		}
	}

	dsn := util.ToString(conf.Get("orm", "acc"))
	mds := orm.NewModels()
	mds.Set(dsn, &db_acc.API{})

	dsn = util.ToString(conf.Get("orm", "order"))
	mds.Set(dsn, &db_order.Order{})

	return orm.Reg(mds)
}

func (a *Account) Start(...interface{}) (err error) {
	return
}

func (a *Account) UnInit() {
}

func (a *Account) Name() string {
	return "account"
}

func (a *Account) Api(uid int64, ex m_common.Exchange) (*db_acc.API, error) {
	api := new(db_acc.API)
	tx := orm.DB().Where("uid = ? AND exchange = ?", uid, int(ex)).First(api)
	if tx.RowsAffected != 1 {
		return nil, define.ErrNotBindApiKey
	}

	return api, nil
}

func Reg(ex Exchange) {
	ins.Reg(ex)
}

func (a *Account) Reg(ex Exchange) {
	a.exchanges[fmt.Sprintf("%d-%d", ex.Ty(), ex.ExTy())] = ex
}

func Sub(ty m_common.Exchange, eTy m_common.ExchangeType, uid int64) (*Subscribed, error) {
	return ins.Sub(ty, eTy, uid)
}

func (a *Account) Sub(ty m_common.Exchange, eTy m_common.ExchangeType, uid int64) (*Subscribed, error) {
	ex, ok := a.exchanges[fmt.Sprintf("%d-%d", ty, eTy)]
	if !ok {
		return nil, define.ErrUnknownExchange
	}

	api, err := a.Api(uid, ty)
	if err != nil {
		return nil, err
	}

	return ex.Sub(api.APIKey, api.APISecret, api.Extended)
}

func UnSub(ty m_common.Exchange, eTy m_common.ExchangeType, id int64) {
	ins.UnSub(ty, eTy, id)
}

func (a *Account) UnSub(ty m_common.Exchange, eTy m_common.ExchangeType, id int64) {
	ex, ok := a.exchanges[fmt.Sprintf("%d-%d", ty, eTy)]
	if !ok {
		return
	}

	ex.UnSub(id)
}

func ChangeSetting(ty m_common.Exchange, eTy m_common.ExchangeType, uid int64, setting []*m_setting.Leverage) error {
	return ins.ChangeSetting(ty, eTy, uid, setting)
}

func (a *Account) ChangeSetting(ty m_common.Exchange, eTy m_common.ExchangeType, uid int64, setting []*m_setting.Leverage) error {
	ex, ok := a.exchanges[fmt.Sprintf("%d-%d", ty, eTy)]
	if !ok {
		return define.ErrUnknownExchange
	}

	api, err := a.Api(uid, ty)
	if err != nil {
		return err
	}

	return ex.ChangeSetting(api.APIKey, api.APISecret, api.Extended, setting)
}
