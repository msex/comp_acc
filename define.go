package account

import (
	"gitlab.com/msex/model_pb/kitex_gen/model/m_balance"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_common"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_order"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_position"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_setting"
)

type Subscribed struct {
	Id       int64
	Position chan []*m_position.Position
	Balance  chan []*m_balance.Balance
	Order    chan *m_order.Order
	Leverage chan []*m_setting.Leverage
	Error    chan error
}

type Exchange interface {
	Ty() m_common.Exchange
	ExTy() m_common.ExchangeType

	Start() error

	Sub(apiKey, secret, extended string) (*Subscribed, error)
	UnSub(subId int64)

	ChangeSetting(apiKey, secret, extended string, setting []*m_setting.Leverage) error
}
